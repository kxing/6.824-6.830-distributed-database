package simpledb.systemtest.paxos;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import simpledb.paxos.Paxos;
import simpledb.paxos.PaxosConfiguration;
import simpledb.paxos.PaxosStatics;

public class PaxosTest {
    private static final int NUMBER_OF_SERVERS = 3;
    private static PaxosConfiguration[] paxosConfigurations;
    private static PaxosStatics[] staticInstances;
    
    @BeforeClass
    public static void setUp() throws IOException {
        paxosConfigurations = new PaxosConfiguration[NUMBER_OF_SERVERS];
        staticInstances = new PaxosStatics[NUMBER_OF_SERVERS];
        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            paxosConfigurations[i] = new PaxosConfiguration(
                    String.format("test_files/server%d.config", i + 1));
            staticInstances[i] =
                    new PaxosStatics(paxosConfigurations[i]);
            staticInstances[i].startRpcListener();
        }
    }

    @Test
    public void testOneProposer() throws InterruptedException {
        staticInstances[0].getPaxos().startPaxos(1, "Hi");
        // Wait for Paxos to finish running.
        Thread.sleep(100);
        
        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            Paxos p = staticInstances[i].getPaxos();
            
            // We can only agree on "Hi".
            Assert.assertTrue(p.containsSequenceNumber(1));
            Assert.assertEquals("Hi", p.getPaxosLogValue(1));
        }
    }

    @Test
    public void testTwoProposers() throws InterruptedException {
        staticInstances[0].getPaxos().startPaxos(2, "1");
        staticInstances[1].getPaxos().startPaxos(2, "2");
        
        // Wait for Paxos to finish running.
        Thread.sleep(100);

        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            Paxos p = staticInstances[i].getPaxos();
            
            // We can only agree on "1" or "2".
            Assert.assertTrue(p.containsSequenceNumber(2));
            String value = p.getPaxosLogValue(2);
            
            boolean validValue = value.equals("1") ||
                                 value.equals("2");
            Assert.assertTrue(validValue);
        }
    }

    @Test
    public void testThreeProposers() throws InterruptedException {
        staticInstances[0].getPaxos().startPaxos(3, "A");
        staticInstances[1].getPaxos().startPaxos(3, "B");
        staticInstances[2].getPaxos().startPaxos(3, "C");
        
        // Wait for Paxos to finish running.
        Thread.sleep(100);

        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            Paxos p = staticInstances[i].getPaxos();
            
            // We can only agree on "A", "B", or "C".
            Assert.assertTrue(p.containsSequenceNumber(3));
            String value = p.getPaxosLogValue(3);
            
            boolean validValue = value.equals("A") ||
                                 value.equals("B") ||
                                 value.equals("C");
            Assert.assertTrue(validValue);
        }
    }
    
    @Test
    public void testForgetting() throws InterruptedException {
        staticInstances[0].getPaxos().startPaxos(4, "Forgetting...");

        // Wait for Paxos to finish running.
        Thread.sleep(100);
        
        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            Paxos p = staticInstances[i].getPaxos();
            
            // We can only agree on "Forgetting...".
            Assert.assertTrue(p.containsSequenceNumber(4));
            Assert.assertEquals("Forgetting...", p.getPaxosLogValue(4));
        }
        
        // Tell everyone to forget the values up to and including sequence
        // number 4.
        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            Paxos p = staticInstances[i].getPaxos();
            p.forgetValues(4);
        }
        
        // Have everybody propose, and find out about the done values.
        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            staticInstances[i].getPaxos().startPaxos(5, "Forgetting test");
        }

        // Wait for Paxos to finish running.
        Thread.sleep(100);
        
        for (int i = 0; i < NUMBER_OF_SERVERS; i++) {
            Paxos p = staticInstances[i].getPaxos();
            
            // We don't remember the value for sequence number 4.
            Assert.assertFalse(p.containsSequenceNumber(4));

            // We can only agree on "Forgetting test".
            Assert.assertTrue(p.containsSequenceNumber(5));
            Assert.assertEquals("Forgetting test", p.getPaxosLogValue(5));
        }
    }
}
