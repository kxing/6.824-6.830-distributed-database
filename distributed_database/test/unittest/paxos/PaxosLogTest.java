package unittest.paxos;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.PaxosException;
import simpledb.paxos.PaxosLog;

public class PaxosLogTest {

    @Test
    public void testAddToLog() {
        PaxosLog pl = new PaxosLog("derp.log");
        pl.addToLog(1, "SELECT * FROM test;");

        Assert.assertFalse(pl.containsSequenceNumber(0));
        Assert.assertTrue(pl.containsSequenceNumber(1));
        Assert.assertFalse(pl.containsSequenceNumber(2));
        Assert.assertFalse(pl.containsSequenceNumber(3));

        pl.addToLog(2, "SELECT * FROM test2;");

        Assert.assertFalse(pl.containsSequenceNumber(0));
        Assert.assertTrue(pl.containsSequenceNumber(1));
        Assert.assertTrue(pl.containsSequenceNumber(2));
        Assert.assertFalse(pl.containsSequenceNumber(3));
        
        // Make sure the log items are stored correctly.
        Assert.assertEquals("SELECT * FROM test;", pl.getValue(1));
        Assert.assertEquals("SELECT * FROM test2;", pl.getValue(2));
        

        // Make sure we can't add item 1 a second time.
        try {
            pl.addToLog(1, "SELECT * FROM test;");
            Assert.fail();
        } catch (PaxosException e) {
            // Expected exception.
        }
    }
    
    @Test
    public void testGetItem() {
        PaxosLog pl = new PaxosLog("derp.log");
        pl.addToLog(1, "SELECT * FROM test;");
        
        // Make sure item 1 is in the log.
        Assert.assertEquals("SELECT * FROM test;", pl.getValue(1));
        
        // Make sure item 0 is not in the log.
        try {
            pl.getValue(0);
            Assert.fail();
        } catch (PaxosException e) {
            // Expected exception.
        }
        
        // Make sure item 2 is not in the log.
        try {
            pl.getValue(2);
            Assert.fail();
        } catch (PaxosException e) {
            // Expected exception.
        }
    }
    
    @Test
    public void testForgetItem() {
        PaxosLog pl = new PaxosLog("derp.log");
        pl.addToLog(1, "SELECT * FROM test;");
        pl.addToLog(2, "SELECT * FROM test2;");

        Assert.assertFalse(pl.containsSequenceNumber(0));
        Assert.assertTrue(pl.containsSequenceNumber(1));
        Assert.assertTrue(pl.containsSequenceNumber(2));
        Assert.assertFalse(pl.containsSequenceNumber(3));
        
        pl.forgetValue(2);
        
        // Check that the Paxos log forgot item 2.
        Assert.assertFalse(pl.containsSequenceNumber(0));
        Assert.assertTrue(pl.containsSequenceNumber(1));
        Assert.assertFalse(pl.containsSequenceNumber(2));
        Assert.assertFalse(pl.containsSequenceNumber(3));
        
        // Check to make sure we can't forget something twice.
        try {
            pl.forgetValue(2);
            Assert.fail();
        } catch (PaxosException e) {
            // Expected exception.
        }
    }
}
