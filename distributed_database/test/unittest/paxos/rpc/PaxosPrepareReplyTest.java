package unittest.paxos.rpc;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.rpc.PaxosPrepareReply;

public class PaxosPrepareReplyTest {

    @Test
    public void testConstructor() {
        PaxosPrepareReply ppr = new PaxosPrepareReply(true, 4, "SELECT * FROM table", -1);
        Assert.assertEquals(true, ppr.isOk());
        Assert.assertEquals(4, ppr.getHighestAcceptNumber());
        Assert.assertEquals("SELECT * FROM table", ppr.getHighestAcceptValue());
        Assert.assertEquals(-1, ppr.getDoneValue());
    }
    
    @Test
    public void testStringConversions() {
        PaxosPrepareReply ppr = new PaxosPrepareReply(true, 4, "SELECT * FROM table", -1);
        String reply = ppr.getRpcReply();
        
        PaxosPrepareReply ppr2 = PaxosPrepareReply.parseReply(reply);
        
        // Make sure the parsed reply is equivalent.
        Assert.assertEquals(ppr.isOk(), ppr2.isOk());
        Assert.assertEquals(ppr.getHighestAcceptNumber(), ppr2.getHighestAcceptNumber());
        Assert.assertEquals(ppr.getHighestAcceptValue(), ppr2.getHighestAcceptValue());
        Assert.assertEquals(ppr.getDoneValue(), ppr2.getDoneValue());
    }
}
