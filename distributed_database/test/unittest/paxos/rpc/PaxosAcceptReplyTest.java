package unittest.paxos.rpc;

import org.junit.Test;
import org.junit.Assert;
import simpledb.paxos.rpc.PaxosAcceptReply;

public class PaxosAcceptReplyTest {

    @Test
    public void testConstructor() {
        PaxosAcceptReply par = new PaxosAcceptReply(true, -1);
        Assert.assertEquals(true, par.isOk());
        Assert.assertEquals(-1, par.getDoneValue());
    }
    
    @Test
    public void testStringConversions() {
        PaxosAcceptReply par = new PaxosAcceptReply(true, -1);
        String reply = par.getRpcReply();
        
        PaxosAcceptReply par2 = PaxosAcceptReply.parseReply(reply);

        // Make sure the parsed reply is equivalent.
        Assert.assertEquals(par.isOk(), par2.isOk());
        Assert.assertEquals(par.getDoneValue(), par2.getDoneValue());
    }
}
