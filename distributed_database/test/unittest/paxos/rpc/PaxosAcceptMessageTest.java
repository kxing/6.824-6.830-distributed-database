package unittest.paxos.rpc;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.rpc.PaxosAcceptMessage;

public class PaxosAcceptMessageTest {

    @Test
    public void testConstructor() {
        PaxosAcceptMessage pam = new PaxosAcceptMessage(1, 4, "SELECT * FROM table");
        
        Assert.assertEquals(1, pam.getSequenceNumber());
        Assert.assertEquals(4, pam.getProposalNumber());
        Assert.assertEquals("SELECT * FROM table", pam.getValue());
    }
    
    @Test
    public void testStringConversions() {
        PaxosAcceptMessage pam = new PaxosAcceptMessage(1, 4, "SELECT * FROM table");
        String message = pam.getRpcMessage();
        
        // Make sure the PaxosAcceptMessage is formatted correctly.
        Assert.assertTrue(PaxosAcceptMessage.matchesRpcMessage(message));
        
        PaxosAcceptMessage pam2 = PaxosAcceptMessage.parseMessage(message);

        // Make sure the parsed version is equivalent.
        Assert.assertEquals(pam.getSequenceNumber(), pam2.getSequenceNumber());
        Assert.assertEquals(pam.getProposalNumber(), pam2.getProposalNumber());
        Assert.assertEquals(pam.getValue(), pam2.getValue());
    }
}
