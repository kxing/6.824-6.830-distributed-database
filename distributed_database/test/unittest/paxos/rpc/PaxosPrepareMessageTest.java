package unittest.paxos.rpc;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.rpc.PaxosPrepareMessage;

public class PaxosPrepareMessageTest {

    @Test
    public void testConstructor() {
        PaxosPrepareMessage ppm = new PaxosPrepareMessage(1, 4);
        Assert.assertEquals(1, ppm.getSequenceNumber());
        Assert.assertEquals(4, ppm.getProposalNumber());
    }
    
    @Test
    public void testStringConversions() {
        PaxosPrepareMessage ppm = new PaxosPrepareMessage(1, 4);
        String message = ppm.getRpcMessage();
        
        // Make sure the PaxosPrepareMessage is formatted correctly.
        Assert.assertTrue(PaxosPrepareMessage.matchesRpcMessage(message));
        
        PaxosPrepareMessage ppm2 = PaxosPrepareMessage.parseMessage(message);
        
        // Make sure the parsed version is equivalent.
        Assert.assertEquals(ppm.getSequenceNumber(), ppm2.getSequenceNumber());
        Assert.assertEquals(ppm.getProposalNumber(), ppm2.getProposalNumber());
    }
}
