package unittest.paxos.rpc;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.rpc.PaxosDecidedReply;

public class PaxosDecidedReplyTest {
    
    @Test
    public void testConstructor() {
        PaxosDecidedReply pdr = new PaxosDecidedReply(-1);
        Assert.assertEquals(-1, pdr.getDoneValue());
    }

    @Test
    public void testStringConversions() {
        PaxosDecidedReply pdr = new PaxosDecidedReply(-1);
        String reply = pdr.getRpcReply();
        
        PaxosDecidedReply pdr2 = PaxosDecidedReply.parseReply(reply);

        // Make sure the parsed reply is equivalent.
        // TODO(kxing): Do a better test than this.
        Assert.assertEquals(pdr.getDoneValue(), pdr2.getDoneValue());
    }
}
