package unittest.paxos.rpc;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.rpc.PaxosDecidedMessage;

public class PaxosDecidedMessageTest {

    @Test
    public void testConstructor() {
        PaxosDecidedMessage pdm = new PaxosDecidedMessage(1, "Hi");
        Assert.assertEquals(1, pdm.getSequenceNumber());
        Assert.assertEquals("Hi", pdm.getValue());
    }
    
    @Test
    public void testStringConversions() {
        PaxosDecidedMessage pdm = new PaxosDecidedMessage(1, "Hi");
        String message = pdm.getRpcMessage();
        
        // Make sure the PaxosAcceptMessage is formatted correctly.
        Assert.assertTrue(PaxosDecidedMessage.matchesRpcMessage(message));
        
        PaxosDecidedMessage pdm2 = PaxosDecidedMessage.parseMessage(message);

        // Make sure the parsed version is equivalent.
        Assert.assertEquals(pdm.getSequenceNumber(), pdm2.getSequenceNumber());
        Assert.assertEquals(pdm.getValue(), pdm2.getValue());
    }
}
