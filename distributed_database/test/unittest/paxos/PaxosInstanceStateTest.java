package unittest.paxos;

import org.junit.Assert;
import org.junit.Test;

import simpledb.paxos.PaxosInstanceState;

public class PaxosInstanceStateTest {

    @Test
    public void testSetHighestPrepareNumber() {
        PaxosInstanceState pis = new PaxosInstanceState();
        pis.setHighestPrepareNumber(1);
        Assert.assertEquals(1, pis.getHighestPrepareNumber());
    }

    @Test
    public void testSetHighestAcceptNumber() {
        PaxosInstanceState pis = new PaxosInstanceState();
        pis.setHighestAcceptNumber(2);
        Assert.assertEquals(2, pis.getHighestAcceptNumber());
    }

    @Test
    public void testSetHighestAcceptValue() {
        PaxosInstanceState pis = new PaxosInstanceState();
        pis.setHighestAcceptValue("SELECT * FROM test");
        Assert.assertEquals("SELECT * FROM test", pis.getHighestAcceptValue());
    }
}
