package unittest.network;

import org.junit.Assert;
import org.junit.Test;

import simpledb.network.NetworkAddressPort;
import simpledb.network.NetworkException;

public class NetworkAddressPortTest {

    @Test
    public void testConstructorNormal() {
        NetworkAddressPort nap = new NetworkAddressPort("18.0.0.0", 80);
        Assert.assertEquals("18.0.0.0", nap.getAddress());
        Assert.assertEquals(80, nap.getPort());
    }

    @Test
    public void testConstructorLocalhost() {
        NetworkAddressPort nap = new NetworkAddressPort("localhost", 80);
        Assert.assertEquals("localhost", nap.getAddress());
        Assert.assertEquals(80, nap.getPort());
    }

    @Test
    public void testConstructorInvalidAddress() {
        try {
            new NetworkAddressPort("145.314.156.0", 80);
            Assert.fail();
        } catch (NetworkException e) {
            // Supposed to end up here.
        }
    }

    @Test
    public void testConstructorInvalidAddress2() {
        try {
            new NetworkAddressPort("xxlocalhostxx", 80);
            Assert.fail();
        } catch (NetworkException e) {
            // Supposed to end up here.
        }
    }
}
