#!/bin/bash

TEST_WRITES=test_writes.sql

# Delete the .sql file if it exists.
rm -f "$TEST_WRITES"

EXISTING_USERS=`wc -l <users.txt`
EXISTING_POSTS=`wc -l <posts.txt`

for i in {1..100}
do

  echo \
"INSERT INTO users
VALUES ($EXISTING_USERS, 'Derp', 'Herp', 'herpderp@mit.edu');" >>"$TEST_WRITES"

  echo \
"INSERT INTO posts
VALUES ($EXISTING_POSTS, $EXISTING_USERS, 'derp');" >>"$TEST_WRITES"

  EXISTING_POSTS=$(($EXISTING_POSTS + 1))

  echo \
"INSERT INTO posts
VALUES ($EXISTING_POSTS, $EXISTING_USERS, 'herp derp');" >>"$TEST_WRITES"

  EXISTING_POSTS=$(($EXISTING_POSTS + 1))

  echo \
"INSERT INTO posts
VALUES ($EXISTING_POSTS, $EXISTING_USERS, 'more herp derp');" >>"$TEST_WRITES"

  EXISTING_POSTS=$(($EXISTING_POSTS + 1))

  EXISTING_USERS=$(($EXISTING_USERS + 1))
done

echo "exit;" >>"$TEST_WRITES"
