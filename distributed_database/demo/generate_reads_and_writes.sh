#!/bin/bash

TEST_READS_AND_WRITES=test_reads_and_writes.sql

# Delete the .sql file if it exists.
rm -f "$TEST_READS_AND_WRITES"

EXISTING_USERS=`wc -l <users.txt`
EXISTING_POSTS=`wc -l <posts.txt`

for i in {1..100}
do

  echo \
"SELECT user_id
FROM users
WHERE email='kxing@mit.edu';" >>"$TEST_READS_AND_WRITES"

  echo \
"SELECT first_name, last_name, message
FROM users, posts
WHERE users.user_id = posts.user_id;" >>"$TEST_READS_AND_WRITES"

  echo \
"SELECT message
FROM users,posts
WHERE users.user_id = posts.user_id AND users.email = 'niwen@mit.edu';" \
    >>"$TEST_READS_AND_WRITES"

  echo \
"SELECT first_name, last_name, email
FROM users
WHERE user_id = 1;" >>"$TEST_READS_AND_WRITES"

  echo \
"INSERT INTO users
VALUES ($EXISTING_USERS, 'Derp', 'Herp', 'herpderp@mit.edu');" \
    >>"$TEST_READS_AND_WRITES"

  echo \
"INSERT INTO posts
VALUES ($EXISTING_POSTS, $EXISTING_USERS, 'derp');" >>"$TEST_READS_AND_WRITES"

  EXISTING_POSTS=$(($EXISTING_POSTS + 1))

  echo \
"INSERT INTO posts
VALUES ($EXISTING_POSTS, $EXISTING_USERS, 'herp derp');" \
    >>"$TEST_READS_AND_WRITES"

  EXISTING_POSTS=$(($EXISTING_POSTS + 1))

  echo \
"INSERT INTO posts
VALUES ($EXISTING_POSTS, $EXISTING_USERS, 'more herp derp');" \
    >>"$TEST_READS_AND_WRITES"

  EXISTING_POSTS=$(($EXISTING_POSTS + 1))

  EXISTING_USERS=$(($EXISTING_USERS + 1))
done

echo "exit;" >>"$TEST_READS_AND_WRITES"
