#!/bin/bash

TEST_READS=test_reads.sql

# Delete the .sql file if it exists.
rm -f "$TEST_READS"

for i in {1..100}
do

  echo \
"SELECT user_id
FROM users
WHERE email='kxing@mit.edu';" >>"$TEST_READS"

  echo \
"SELECT first_name, last_name, message
FROM users, posts
WHERE users.user_id = posts.user_id;" >>"$TEST_READS"

  echo \
"SELECT message
FROM users,posts
WHERE users.user_id = posts.user_id AND users.email = 'niwen@mit.edu';" >>"$TEST_READS"

  echo \
"SELECT first_name, last_name, email
FROM users
WHERE user_id = 1;" >>"$TEST_READS"
done

echo "exit;" >>"$TEST_READS"
