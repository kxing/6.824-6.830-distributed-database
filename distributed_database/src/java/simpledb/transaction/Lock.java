package simpledb.transaction;

import java.util.HashSet;
import java.util.Random;

/**
 * A reentrant lock, that keeps track of transaction IDs.
 * 
 * @author kxing
 */
public class Lock {
    
    private HashSet<TransactionId> sharedLockHolders;
    private TransactionId exclusiveLockHolder;
    
    // For sleeping jitter.
    private final Random random;
    
    // If we fail to acquire a lock, we will sleep for 10ms before trying again.
    private static final int SLEEP_DURATION = 10;
    // If we fail to acquire a lock within the timeout period, then give up.
    private static final int DEADLOCK_TIMEOUT = 150;
    
    private static final boolean USES_HOMICIDAL_DEADLOCK_RESOLUTION = false;
    
    // The victim transactions that will get aborted if they block for too long.
    private static final HashSet<TransactionId> victims;
    
    static {
        victims = new HashSet<TransactionId>();
    }
    
    private static void addVictim(TransactionId victim) {
        synchronized (victims) {
            victims.add(victim);
        }
    }
    
    public Lock() {
        this.sharedLockHolders = new HashSet<TransactionId>();
        this.exclusiveLockHolder = null;
        this.random = new Random();
    }
    
    /**
     * Acquires the lock with the specified permissions.
     * Waits until the lock can be acquired.
     * 
     * @param transactionId The ID of the transaction.
     * @param permissions The type of lock requested.
     */
    public void acquireLock(TransactionId transactionId,
                            Permissions permissions)
            throws TransactionAbortedException {
        
        long currentTime = System.currentTimeMillis();
        while (true) {
            // Return, if we succeeded in acquiring the lock.
            if (tryToAcquireLock(transactionId, permissions)) {
                return;
            }
            if (System.currentTimeMillis() - currentTime >= DEADLOCK_TIMEOUT) {
                if (!USES_HOMICIDAL_DEADLOCK_RESOLUTION) {
                    throw new TransactionAbortedException();
                } else {
                    synchronized (victims) {
                        // Abort if we are a victim.
                        if (victims.contains(transactionId)) {
                            throw new TransactionAbortedException();
                        }
                    }
                    this.killCompetingTransactions(transactionId, permissions);
                }
            }
            try {
                Thread.sleep(SLEEP_DURATION + this.random.nextInt(SLEEP_DURATION));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Tries to acquire the lock, with the given permissions, and returns
     * whether the lock acquisition was successful.
     * 
     * @param transactionId The ID of the transaction.
     * @param permissions The type of lock requested.
     * @return Whether the lock acquisition was successful.
     */
    private synchronized boolean tryToAcquireLock(TransactionId transactionId,
                                                  Permissions permissions) {
        if (permissions == Permissions.READ_ONLY) {
            if (exclusiveLockHolder == null) {
                // We can acquire a read lock, since no one has a write lock.
                if (!this.sharedLockHolders.contains(transactionId)) {
                    this.sharedLockHolders.add(transactionId);
                }
                return true;
            } else if (this.exclusiveLockHolder != null &&
                       this.exclusiveLockHolder.equals(transactionId)) {
                // We can acquire a read lock, since we already have the write
                // lock.
                return true;
            }
        } else if (permissions == Permissions.READ_WRITE) {
            if (this.exclusiveLockHolder != null &&
                    this.exclusiveLockHolder.equals(transactionId)) {
                // We can acquire a write lock, since we already have the write
                // lock.
                return true;
            } else if (this.sharedLockHolders.size() == 0 &&
                       this.exclusiveLockHolder == null) {
                // We can acquire a write lock, since no one has a read lock.
                this.exclusiveLockHolder = transactionId;
                return true;
            } else if (this.sharedLockHolders.size() == 1 &&
                       this.sharedLockHolders.contains(transactionId) &&
                       this.exclusiveLockHolder == null) {
                // We can upgrade from a read lock to a write lock, since no
                // one else has a read lock.
                this.sharedLockHolders.remove(transactionId);
                this.exclusiveLockHolder = transactionId;
                return true;
            }
        }
        return false;
    }
    
    private synchronized void killCompetingTransactions(
            TransactionId transactionId, Permissions permissions) {
        // Kill the shared lock holders.
        for (TransactionId lockHolder : this.sharedLockHolders) {
            if (lockHolder.equals(transactionId)) {
                // Don't abort yourself.
                continue;
            }
            addVictim(lockHolder);
        }
        
        // Kill the exclusive lock holders.
        if (this.exclusiveLockHolder != null &&
                !this.exclusiveLockHolder.equals(transactionId)) {
            addVictim(this.exclusiveLockHolder);
        }
    }
    
    /**
     * Releases the lock.
     * 
     * @param transactionId The ID of the transaction.
     */
    public synchronized void releaseLock(TransactionId transactionId) {
        boolean wasSharedLock = this.sharedLockHolders.remove(transactionId);
        boolean wasExclusiveLock = (this.exclusiveLockHolder != null &&
                this.exclusiveLockHolder.equals(transactionId));
        this.exclusiveLockHolder = null;
        
        if (wasSharedLock == wasExclusiveLock) {
            throw new AssertionError("Transaction either did not acquire a " +
                    "lock or acquired both shared and exclusive locks");
        }
    }
    
    /**
     * Returns whether the transaction is holding a lock.
     * 
     * @param transactionId The ID of the transaction.
     * @return Whether the transaction is holding a lock.
     */
    public synchronized boolean holdsLock(TransactionId transactionId) {
        return (this.sharedLockHolders.contains(transactionId) ||
                (this.exclusiveLockHolder != null &&
                 this.exclusiveLockHolder.equals(transactionId)));
    }
}
