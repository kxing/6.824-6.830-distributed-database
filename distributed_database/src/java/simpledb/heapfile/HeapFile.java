package simpledb.heapfile;

import java.io.*;
import java.util.*;

import simpledb.infrastructure.BufferPool;
import simpledb.infrastructure.Database;
import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbFile;
import simpledb.infrastructure.DbFileIterator;
import simpledb.infrastructure.Page;
import simpledb.infrastructure.PageId;
import simpledb.transaction.Permissions;
import simpledb.transaction.TransactionAbortedException;
import simpledb.transaction.TransactionId;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.heapfile.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {
    
    private final File file;
    private final TupleDesc tupleDescription;

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
    public HeapFile(File f, TupleDesc td) {
        this.file = f;
        this.tupleDescription = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        return this.file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        return this.tupleDescription;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // Check that we are trying to read from a HeapPageId page.
        if (!(pid instanceof HeapPageId)) {
            throw new AssertionError(
                    "PageId " + pid.toString() + " is not a HeapPageId.");
        }
        
        HeapPageId pageId = (HeapPageId)(pid);
        try {
            // Open the file.
            RandomAccessFile raf = new RandomAccessFile(this.file, "r");
            // Find the right location.
            raf.seek(pid.pageNumber() * BufferPool.getPageSize());
            
            // Create a buffer and fill it.
            byte[] data = new byte[BufferPool.getPageSize()];
            int bytesRead = raf.read(data);
            if (bytesRead != BufferPool.getPageSize()) {
                throw new AssertionError(
                        "PageId " + pid.toString() + " could not be read.");
            }
            
            // Create the page.
            Page p = new HeapPage(pageId, data);
            
            // Clean up and return.
            raf.close();
            return p;
        } catch (IOException e) {
            // If we can't read from the file, we're screwed.
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // Open the file for writing.
        RandomAccessFile randomAccessFile = new RandomAccessFile(this.file, "rw");
        
        // Move the pointer to the correct location.
        randomAccessFile.seek(page.getId().pageNumber() * BufferPool.getPageSize());
        
        // Write the page's data to disk.
        randomAccessFile.write(page.getPageData());
        
        // Clean up.
        randomAccessFile.close();
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        return (int)(this.file.length() / BufferPool.getPageSize());
    }
    
    /**
     * Extends the file by one page.
     * @throws IOException If we fail to extend the file.
     */
    private void extendFile() throws IOException {
        // Open the file for appending.
        FileOutputStream fos = new FileOutputStream(this.file, true);
        
        // Create a buffer of zeroes.
        byte[] zeroes = new byte[BufferPool.getPageSize()];
        Arrays.fill(zeroes, (byte)(0));
        
        // Write out the file, and close it.
        fos.write(zeroes);
        fos.close();
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // Container for the pages that we modify.
        ArrayList<Page> modifiedPages = new ArrayList<Page>();
        
        int tableId = this.getId();
        int numberOfPages = this.numPages();
        
        // Try to see if there is room on each page.
        for (int pageNumber = 0; pageNumber < numberOfPages; pageNumber++) {
            // Fetch the page from the buffer pool.
            Page page = Database.getBufferPool().getPage(
                    tid,
                    new HeapPageId(tableId, pageNumber),
                    Permissions.READ_WRITE);
            if (!(page instanceof HeapPage)) {
                throw new AssertionError("Page in HeapFile is not a HeapPage.");
            }
            HeapPage heapPage = (HeapPage)(page);
            
            boolean success = true;
            try {
                // Try to insert the tuple into the page.
                heapPage.insertTuple(t);
            } catch (DbException e) {
                // Do nothing, and try again with the next page.
                success = false;
            }
            
            if (success) {
                // Return the page that was modified.
                modifiedPages.add(heapPage);
                return modifiedPages;
            }
        }
        
        // Make sure we can't extend the file concurrently.
        // TODO(kxing): Support more concurrency.
        synchronized (this) {
            // We can't insert into any of the pages, so we have to extend the file.
            this.extendFile();
            
            // Fetch the new page from the buffer pool.
            Page newPage = Database.getBufferPool().getPage(
                    tid,
                    new HeapPageId(tableId, this.numPages() - 1),
                    Permissions.READ_WRITE);
            if (!(newPage instanceof HeapPage)) {
                throw new AssertionError("Page in HeapFile is not a HeapPage.");
            }
            HeapPage newHeapPage = (HeapPage)(newPage);
            
            // Insert the tuple into the page.
            newHeapPage.insertTuple(t);
            
            // Return the page that was modified.
            modifiedPages.add(newHeapPage);
        }
        return modifiedPages;
    }

    // see DbFile.java for javadocs
    public Page deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // Fetch the page from the buffer pool.
        Page page = Database.getBufferPool().getPage(
                tid, 
                t.getRecordId().getPageId(),
                Permissions.READ_WRITE);
        if (!(page instanceof HeapPage)) {
            throw new AssertionError("Page in HeapFile is not a HeapPage.");
        }
        HeapPage heapPage = (HeapPage)(page);
        
        // Delete the tuple from the page.
        heapPage.deleteTuple(t);
        
        return heapPage;
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        return new HeapFileIterator(this, tid);
    }
    
    private class HeapFileIterator implements DbFileIterator {
        private final HeapFile heapFile;
        private final TransactionId transactionId;
        
        private boolean open;
        private boolean closed;
        
        // Invariants:
        //      0 <= currentPage < heapFile.numOfPages()
        //      currentPageIterator corresponds to the current page.
        //      currentPageIterator points to the next tuple, if there is one.
        private int currentPageIndex;
        private Iterator<Tuple> currentPageTupleIterator;
        
        public HeapFileIterator(HeapFile heapFile, TransactionId tid) {
            this.heapFile = heapFile;
            this.transactionId = tid;
            
            this.open = false;
            this.closed = false;
            
            this.currentPageIndex = 0;
            this.currentPageTupleIterator = null;
        }
        
        private void loadCurrentPageIterator()
                throws DbException, TransactionAbortedException {
            HeapPageId pageId = new HeapPageId(heapFile.getId(),
                                               this.currentPageIndex);
            
            // Load the page.
            Page page = Database.getBufferPool().getPage(
                    this.transactionId, pageId, Permissions.READ_ONLY);
            
            if (!(page instanceof HeapPage)) {
                throw new AssertionError("The page is not a heap page.");
            }
            
            HeapPage heapPage = (HeapPage)(page);
            
            // Retrieve the iterator.
            this.currentPageTupleIterator = heapPage.iterator();
        }

        @Override
        public void close() {
            this.open = false;
            this.closed = true;
        }

        @Override
        public boolean hasNext() throws DbException,
                TransactionAbortedException {
            if (!this.open || this.closed) {
                return false;
            }
            
            if (this.currentPageTupleIterator == null) {
                this.loadCurrentPageIterator();
            }
            return this.currentPageTupleIterator.hasNext();
        }

        @Override
        public Tuple next() throws DbException, TransactionAbortedException,
                NoSuchElementException {
            if (!this.open) {
                throw new NoSuchElementException(
                        "HeapFileIterator is not open.");
            }
            if (this.closed) {
                throw new NoSuchElementException(
                        "HeapFileIterator was closed.");
            }
            
            if (!this.hasNext()) {
                throw new NoSuchElementException("No more tuples.");
            }
            
            // Extract the tuple to be returned.
            if (this.currentPageTupleIterator == null) {
                this.loadCurrentPageIterator();
            }
            Tuple tuple = this.currentPageTupleIterator.next();
            
            // Fix up the iterator, if necessary.
            while (!this.currentPageTupleIterator.hasNext() &&
                    this.currentPageIndex + 1 < this.heapFile.numPages()) {
                this.currentPageIndex++;
                this.loadCurrentPageIterator();
            }
            
            return tuple;
        }

        @Override
        public void open() throws DbException, TransactionAbortedException {
            this.open = true;
            this.closed = false;
        }

        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            if (!this.open) {
                throw new AssertionError(
                        "HeapFileIterator is not open.");
            }
            this.currentPageIndex = 0;
            this.loadCurrentPageIterator();
        }
        
    }

}

