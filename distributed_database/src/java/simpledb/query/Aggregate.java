package simpledb.query;

import java.util.*;

import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbIterator;
import simpledb.transaction.TransactionAbortedException;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;
import simpledb.tuple.Type;

/**
 * The Aggregation operator that computes an aggregate (e.g., sum, avg, max,
 * min). Note that we only support aggregates over a single column, grouped by a
 * single column.
 */
public class Aggregate extends Operator {

    private static final long serialVersionUID = 1L;
    
    private DbIterator tupleIterator;
    private final int aggregationFieldIndex;
    private final int groupByFieldIndex;
    private final Aggregator.Op operator;

    private DbIterator aggregateTupleIterator;

    /**
     * Constructor.
     * 
     * Implementation hint: depending on the type of afield, you will want to
     * construct an {@link IntAggregator} or {@link StringAggregator} to help
     * you with your implementation of readNext().
     * 
     * 
     * @param child
     *            The DbIterator that is feeding us tuples.
     * @param afield
     *            The column over which we are computing an aggregate.
     * @param gfield
     *            The column over which we are grouping the result, or -1 if
     *            there is no grouping
     * @param aop
     *            The aggregation operator to use
     */
    public Aggregate(DbIterator child, int afield, int gfield, Aggregator.Op aop) {
        this.tupleIterator = child;
        this.aggregationFieldIndex = afield;
        this.groupByFieldIndex = gfield;
        this.operator = aop;
        this.aggregateTupleIterator = null;
    }

    /**
     * @return If this aggregate is accompanied by a groupby, return the groupby
     *         field index in the <b>INPUT</b> tuples. If not, return
     *         {@link simpledb.query.Aggregator#NO_GROUPING}
     * */
    public int groupField() {
        return this.groupByFieldIndex;
    }

    /**
     * @return If this aggregate is accompanied by a group by, return the name
     *         of the groupby field in the <b>OUTPUT</b> tuples If not, return
     *         null;
     * */
    public String groupFieldName() {
        if (this.groupByFieldIndex == Aggregator.NO_GROUPING) {
            return null;
        }
        
        TupleDesc tupleDescriptor = this.tupleIterator.getTupleDesc();
        return tupleDescriptor.getFieldName(this.groupByFieldIndex);
    }

    /**
     * @return the aggregate field
     * */
    public int aggregateField() {
        return this.aggregationFieldIndex;
    }

    /**
     * @return return the name of the aggregate field in the <b>OUTPUT</b>
     *         tuples
     * */
    public String aggregateFieldName() {
        TupleDesc tupleDescriptor = this.tupleIterator.getTupleDesc();
        return tupleDescriptor.getFieldName(this.aggregationFieldIndex);
    }

    /**
     * @return return the aggregate operator
     * */
    public Aggregator.Op aggregateOp() {
        return this.operator;
    }

    public static String nameOfAggregatorOp(Aggregator.Op aop) {
        return aop.toString();
    }
    
    private void initializeAggregateTupleIterator() throws
            NoSuchElementException, TransactionAbortedException, DbException {
        TupleDesc tupleDescriptor = this.tupleIterator.getTupleDesc();
        
        // Get the aggregation field.
        Type aggregationFieldType =
                tupleDescriptor.getFieldType(this.aggregationFieldIndex);
        
        // Get the group by field, if one exists.
        Type groupByFieldType = null;
        if (this.groupByFieldIndex != Aggregator.NO_GROUPING) {
            groupByFieldType = tupleDescriptor.getFieldType(this.groupByFieldIndex);
        }
        
        // Make an aggregator.
        Aggregator aggregator = null;
        
        switch (aggregationFieldType) {
        case INT_TYPE:
            aggregator = new IntegerAggregator(
                    this.groupByFieldIndex,
                    groupByFieldType,
                    this.aggregationFieldIndex,
                    this.operator);
            break;
        case STRING_TYPE:
            aggregator = new StringAggregator(
                    this.groupByFieldIndex,
                    groupByFieldType,
                    this.aggregationFieldIndex,
                    this.operator);
            break;
        default:
            throw new AssertionError(String.format(
                    "Unsupported type for aggregation: %s",
                    aggregationFieldType.toString()));
        }
        
        // Feed in all the tuples into the aggregator.
        while (this.tupleIterator.hasNext()) {
            aggregator.mergeTupleIntoGroup(this.tupleIterator.next());
        }
        
        // Get back an iterator for the aggregator.
        this.aggregateTupleIterator = aggregator.iterator();
    }

    public void open() throws NoSuchElementException, DbException,
	        TransactionAbortedException {
        
        this.tupleIterator.open();
        
        if (this.aggregateTupleIterator == null) {
            this.initializeAggregateTupleIterator();
            this.aggregateTupleIterator.open();
        }
        super.open();
    }

    /**
     * Returns the next tuple. If there is a group by field, then the first
     * field is the field by which we are grouping, and the second field is the
     * result of computing the aggregate, If there is no group by field, then
     * the result tuple should contain one field representing the result of the
     * aggregate. Should return null if there are no more tuples.
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        if (!this.aggregateTupleIterator.hasNext()) {
            return null;
        }
        return this.aggregateTupleIterator.next();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        this.aggregateTupleIterator.rewind();
    }

    /**
     * Returns the TupleDesc of this Aggregate. If there is no group by field,
     * this will have one field - the aggregate column. If there is a group by
     * field, the first field will be the group by field, and the second will be
     * the aggregate value column.
     * 
     * The name of an aggregate column should be informative. For example:
     * "aggName(aop) (child_td.getFieldName(afield))" where aop and afield are
     * given in the constructor, and child_td is the TupleDesc of the child
     * iterator.
     */
    public TupleDesc getTupleDesc() {
        TupleDesc tupleDescriptor = this.tupleIterator.getTupleDesc();
        
        Type[] types = null;
        if (this.groupByFieldIndex == Aggregator.NO_GROUPING) {
            types = new Type[] {
                    tupleDescriptor.getFieldType(this.aggregationFieldIndex),
            };
        } else {
            types = new Type[] {
                    tupleDescriptor.getFieldType(this.groupByFieldIndex),
                    tupleDescriptor.getFieldType(this.aggregationFieldIndex),
            };
        }
        
        String[] names = null;
        if (this.groupByFieldIndex == Aggregator.NO_GROUPING) {
            names = new String[] {
                    String.format("%s(%s)",
                            this.operator.toString(),
                            tupleDescriptor.getFieldName(this.aggregationFieldIndex)),
            };
        } else {
            names = new String[] {
                    tupleDescriptor.getFieldName(this.groupByFieldIndex),
                    String.format("%s(%s)",
                            this.operator.toString(),
                            tupleDescriptor.getFieldName(this.aggregationFieldIndex)),
            };
        }
        return new TupleDesc(types, names);
    }

    public void close() {
        super.close();
        this.tupleIterator.close();
        this.aggregateTupleIterator.close();
        this.aggregateTupleIterator = null;
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[] {this.tupleIterator};
    }

    @Override
    public void setChildren(DbIterator[] children) {
        if (children.length != 1) {
            throw new AssertionError("setChildren() can only take one child.");
        }
        this.tupleIterator = children[0];
    }
    
}
