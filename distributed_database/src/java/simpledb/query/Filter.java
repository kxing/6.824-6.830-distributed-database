package simpledb.query;

import java.util.*;

import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbIterator;
import simpledb.transaction.TransactionAbortedException;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;

/**
 * Filter is an operator that implements a relational select.
 */
public class Filter extends Operator {

    private static final long serialVersionUID = 1L;
    
    private final Predicate predicate;
    private DbIterator iterator;

    /**
     * Constructor accepts a predicate to apply and a child operator to read
     * tuples to filter from.
     * 
     * @param p
     *            The predicate to filter tuples with
     * @param child
     *            The child operator
     */
    public Filter(Predicate p, DbIterator child) {
        this.predicate = p;
        this.iterator = child;
    }

    public Predicate getPredicate() {
        return this.predicate;
    }

    public TupleDesc getTupleDesc() {
        return this.iterator.getTupleDesc();
    }

    public void open() throws DbException, NoSuchElementException,
            TransactionAbortedException {
        this.iterator.open();
        super.open();
    }

    public void close() {
        super.close();
        this.iterator.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        this.iterator.rewind();
    }

    /**
     * AbstractDbIterator.readNext implementation. Iterates over tuples from the
     * child operator, applying the predicate to them and returning those that
     * pass the predicate (i.e. for which the Predicate.filter() returns true.)
     * 
     * @return The next tuple that passes the filter, or null if there are no
     *         more tuples
     * @see Predicate#filter
     */
    protected Tuple fetchNext() throws NoSuchElementException,
            TransactionAbortedException, DbException {
        while (this.iterator.hasNext()) {
            // Get the next tuple returned by the iterator.
            Tuple tuple = this.iterator.next();

            // Return the tuple if it passes the filter.
            if (this.predicate.filter(tuple)) {
                return tuple;
            }
        }
        return null;
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[]{this.iterator};
    }

    @Override
    public void setChildren(DbIterator[] children) {
        if (children.length != 1) {
            throw new AssertionError(String.format(
                    "Can only set one child. " +
                    "Received %d children",
                    children.length));
        }
        this.iterator = children[0];
    }

}
