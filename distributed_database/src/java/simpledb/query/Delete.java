package simpledb.query;

import simpledb.infrastructure.BufferPool;
import simpledb.infrastructure.Database;
import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbIterator;
import simpledb.transaction.TransactionAbortedException;
import simpledb.transaction.TransactionId;
import simpledb.tuple.IntField;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;
import simpledb.tuple.Type;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator {

    private static final long serialVersionUID = 1L;
    
    private final TransactionId transactionId;
    private DbIterator tupleIterator;
    private boolean called;

    /**
     * Constructor specifying the transaction that this delete belongs to as
     * well as the child to read from.
     * 
     * @param t
     *            The transaction this delete runs in
     * @param child
     *            The child operator from which to read tuples for deletion
     */
    public Delete(TransactionId t, DbIterator child) {
        this.transactionId = t;
        this.tupleIterator = child;
        this.called = false;
    }

    public TupleDesc getTupleDesc() {
        return new TupleDesc(new Type[]{Type.INT_TYPE});
    }

    public void open() throws DbException, TransactionAbortedException {
        this.tupleIterator.open();
        super.open();
    }

    public void close() {
        super.close();
        this.tupleIterator.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        this.tupleIterator.rewind();
    }

    /**
     * Deletes tuples as they are read from the child operator. Deletes are
     * processed via the buffer pool (which can be accessed via the
     * Database.getBufferPool() method.
     * 
     * @return A 1-field tuple containing the number of deleted records.
     * @see Database#getBufferPool
     * @see BufferPool#deleteTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        if (this.called) {
            return null;
        }
        
        int numberOfDeletions = 0;
        
        while (this.tupleIterator.hasNext()) {
            Tuple nextTuple = this.tupleIterator.next();
            Database.getBufferPool().deleteTuple(this.transactionId, nextTuple);
            numberOfDeletions++;
        }
        
        // Remember that we have been called.
        this.called = true;
        
        // Return the results as a tuple.
        Tuple tuple = new Tuple(this.getTupleDesc());
        tuple.setField(0, new IntField(numberOfDeletions));
        return tuple;
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[]{this.tupleIterator};
    }

    @Override
    public void setChildren(DbIterator[] children) {
        if (children.length != 1) {
            throw new AssertionError("Delete takes only one DbIterator.");
        }
        this.tupleIterator = children[0];
    }

}
