package simpledb.query;

import java.util.*;

import simpledb.infrastructure.Database;
import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbFile;
import simpledb.infrastructure.DbFileIterator;
import simpledb.infrastructure.DbIterator;
import simpledb.transaction.TransactionAbortedException;
import simpledb.transaction.TransactionId;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;
import simpledb.tuple.Type;

/**
 * SeqScan is an implementation of a sequential scan access method that reads
 * each tuple of a table in no particular order (e.g., as they are laid out on
 * disk).
 */
public class SeqScan implements DbIterator {

    private static final long serialVersionUID = 1L;
    
    private final TransactionId transactionId;
    
    private boolean open;
    private boolean closed;
    
    private int tableId;
    private String tableAlias;
    
    private DbFileIterator iterator;

    /**
     * Creates a sequential scan over the specified table as a part of the
     * specified transaction.
     * 
     * @param tid
     *            The transaction this scan is running as a part of.
     * @param tableid
     *            the table to scan.
     * @param tableAlias
     *            the alias of this table (needed by the parser); the returned
     *            tupleDesc should have fields with name tableAlias.fieldName
     *            (note: this class is not responsible for handling a case where
     *            tableAlias or fieldName are null. It shouldn't crash if they
     *            are, but the resulting name can be null.fieldName,
     *            tableAlias.null, or null.null).
     */
    public SeqScan(TransactionId tid, int tableid, String tableAlias) {
        this.transactionId = tid;
        
        this.open = false;
        this.closed = true;
        
        this.tableId = tableid;
        this.tableAlias = tableAlias;
    }

    /**
     * @return
     *       return the table name of the table the operator scans. This should
     *       be the actual name of the table in the catalog of the database
     * */
    public String getTableName() {
        return Database.getCatalog().getTableName(this.tableId);
    }
    
    /**
     * @return Return the alias of the table this operator scans. 
     * */
    public String getAlias() {
        return this.tableAlias;
    }

    /**
     * Reset the tableid, and tableAlias of this operator.
     * @param tableid
     *            the table to scan.
     * @param tableAlias
     *            the alias of this table (needed by the parser); the returned
     *            tupleDesc should have fields with name tableAlias.fieldName
     *            (note: this class is not responsible for handling a case where
     *            tableAlias or fieldName are null. It shouldn't crash if they
     *            are, but the resulting name can be null.fieldName,
     *            tableAlias.null, or null.null).
     */
    public void reset(int tableid, String tableAlias) {
        this.tableId = tableid;
        this.tableAlias = tableAlias;
    }

    public SeqScan(TransactionId tid, int tableid) {
        this(tid, tableid, Database.getCatalog().getTableName(tableid));
    }

    public void open() throws DbException, TransactionAbortedException {
        if (this.open) {
            throw new AssertionError("Already open.");
        }
        this.open = true;
        this.closed = false;
        
        this.rewind();
    }

    /**
     * Returns the TupleDesc with field names from the underlying HeapFile,
     * prefixed with the tableAlias string from the constructor. This prefix
     * becomes useful when joining tables containing a field(s) with the same
     * name.
     * 
     * @return the TupleDesc with field names from the underlying HeapFile,
     *         prefixed with the tableAlias string from the constructor.
     */
    public TupleDesc getTupleDesc() {
        TupleDesc schema = Database.getCatalog().getTupleDesc(this.tableId);
        
        Type[] types = new Type[schema.numFields()];
        String[] names = new String[schema.numFields()];
        
        for (int i = 0; i < schema.numFields(); i++) {
            types[i] = schema.getFieldType(i);
            names[i] = "" + this.tableAlias + "." + schema.getFieldName(i);
        }
        
        return (new TupleDesc(types, names));
    }

    public boolean hasNext() throws TransactionAbortedException, DbException {
        if (!this.open || this.closed) {
            return false;
        }
        return this.iterator.hasNext();
    }

    public Tuple next() throws NoSuchElementException,
            TransactionAbortedException, DbException {
        if (!this.open) {
            throw new NoSuchElementException("open() has not been called.");
        }
        if (this.closed) {
            throw new NoSuchElementException("closed() has been called.");
        }
        
        if (!this.hasNext()) {
            throw new AssertionError("No more tuples.");
        }
        
        return this.iterator.next();
    }

    public void close() {
        this.open = false;
        this.closed = true;
        
        // Close the iterator, if we still have one.
        if (this.iterator != null) {
            this.iterator.close();
            this.iterator = null;
        }
    }

    public void rewind() throws DbException, NoSuchElementException,
            TransactionAbortedException {
        if (!this.open) {
            throw new AssertionError("open() has not been called.");
        }
        if (this.closed) {
            throw new AssertionError("closed() has been called.");
        }
        
        // Close the iterator, if we still have one.
        if (this.iterator != null) {
            this.iterator.close();
        }
        
        // Fetch the iterator.
        DbFile databaseFile =
                Database.getCatalog().getDatabaseFile(this.tableId);
        this.iterator = databaseFile.iterator(this.transactionId);
        
        // Open the iterator.
        this.iterator.open();
    }
}
