package simpledb.query;

import java.io.IOException;

import simpledb.infrastructure.BufferPool;
import simpledb.infrastructure.Database;
import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbIterator;
import simpledb.transaction.TransactionAbortedException;
import simpledb.transaction.TransactionId;
import simpledb.tuple.IntField;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;
import simpledb.tuple.Type;

/**
 * Inserts tuples read from the child operator into the tableid specified in the
 * constructor
 */
public class Insert extends Operator {

    private static final long serialVersionUID = 1L;
    
    private final TransactionId transactionId;
    private DbIterator tupleIterator;
    private int tableId;
    private boolean called;

    /**
     * Constructor.
     * 
     * @param t
     *            The transaction running the insert.
     * @param child
     *            The child operator from which to read tuples to be inserted.
     * @param tableid
     *            The table in which to insert tuples.
     * @throws DbException
     *             if TupleDesc of child differs from table into which we are to
     *             insert.
     */
    public Insert(TransactionId t,DbIterator child, int tableid)
            throws DbException {
        this.transactionId = t;
        this.tupleIterator = child;
        this.tableId = tableid;
        this.called = false;
    }

    public TupleDesc getTupleDesc() {
        return new TupleDesc(new Type[]{Type.INT_TYPE});
    }

    public void open() throws DbException, TransactionAbortedException {
        this.tupleIterator.open();
        super.open();
    }

    public void close() {
        super.close();
        this.tupleIterator.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        this.tupleIterator.rewind();
    }

    /**
     * Inserts tuples read from child into the tableid specified by the
     * constructor. It returns a one field tuple containing the number of
     * inserted records. Inserts should be passed through BufferPool. An
     * instances of BufferPool is available via Database.getBufferPool(). Note
     * that insert DOES NOT need check to see if a particular tuple is a
     * duplicate before inserting it.
     * 
     * @return A 1-field tuple containing the number of inserted records, or
     *         null if called more than once.
     * @see Database#getBufferPool
     * @see BufferPool#insertTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        if (this.called) {
            return null;
        }
        
        int tuplesInserted = 0;
        while (this.tupleIterator.hasNext()) {
            Tuple nextTuple = this.tupleIterator.next();
            
            try {
                Database.getBufferPool().insertTuple(
                        this.transactionId,
                        tableId,
                        nextTuple);
            } catch (IOException e) {
                // We tried and failed to insert a tuple.
                throw new DbException(e.getMessage());
            }

            tuplesInserted++;
        }
        
        // Remind ourself that we've been called.
        this.called = true;
        
        // Return the results as a tuple.
        Tuple tuple =  new Tuple(this.getTupleDesc());
        tuple.setField(0, new IntField(tuplesInserted));
        return tuple;
    }

    @Override
    public DbIterator[] getChildren() {
        return new DbIterator[]{this.tupleIterator};
    }

    @Override
    public void setChildren(DbIterator[] children) {
        if (children.length != 1) {
            throw new AssertionError("Insert takes only one DbIterator.");
        }
        this.tupleIterator = children[0];
    }
}
