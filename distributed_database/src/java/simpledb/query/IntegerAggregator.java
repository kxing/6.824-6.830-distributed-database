package simpledb.query;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import simpledb.infrastructure.DbException;
import simpledb.infrastructure.DbIterator;
import simpledb.transaction.TransactionAbortedException;
import simpledb.tuple.Field;
import simpledb.tuple.IntField;
import simpledb.tuple.Tuple;
import simpledb.tuple.TupleDesc;
import simpledb.tuple.Type;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;
    
    private final int groupByFieldIndex;
    private final Type groupByFieldType;
    private final int aggregationFieldIndex;
    private final Op operator;
    
    private final HashMap<Field, IntField> aggregates;
    private final HashMap<Field, Integer> counts;

    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */

    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        this.groupByFieldIndex = gbfield;
        this.groupByFieldType = gbfieldtype;
        this.aggregationFieldIndex = afield;
        this.operator = what;
        
        this.aggregates = new HashMap<Field, IntField>();
        this.counts = new HashMap<Field, Integer>();
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        Field keyValue = null;
        if (this.groupByFieldIndex != NO_GROUPING) {
            keyValue = tup.getField(this.groupByFieldIndex);
        }
        IntField aggregationValue =
            (IntField)(tup.getField(this.aggregationFieldIndex));
        if (!this.counts.containsKey(keyValue)) {
            // A new value.
            this.counts.put(keyValue, 1);
            switch (this.operator) {
            case MIN:
            case MAX:
            case SUM:
            case AVG:
                this.aggregates.put(keyValue, aggregationValue);
                break;
            case COUNT:
                this.aggregates.put(keyValue, new IntField(1));
                break;
            default:
                throw new AssertionError(String.format(
                        "Operator not supported %s", this.operator));
            }
        } else {
            // An existing value.
            this.counts.put(keyValue, this.counts.get(keyValue) + 1);
            
            IntField oldValue = this.aggregates.get(keyValue);
            IntField newValue = oldValue;
            
            switch (this.operator) {
            case MIN:
                if (aggregationValue.compare(
                        Predicate.Op.LESS_THAN, oldValue)) {
                    newValue = aggregationValue;
                }
                break;
            case MAX:
                if (aggregationValue.compare(
                        Predicate.Op.GREATER_THAN, oldValue)) {
                    newValue = aggregationValue;
                }
                break;
            case SUM:
            case AVG:
                int sum = aggregationValue.getValue() +
                          oldValue.getValue();
                newValue = new IntField(sum);
                break;
            case COUNT:
                newValue = new IntField(oldValue.getValue() + 1);
                break;
            default:
                throw new AssertionError(String.format(
                        "Operator not supported %s", this.operator));
            }
            this.aggregates.put(keyValue, newValue);
        }
    }

    /**
     * Create a DbIterator over group aggregate results.
     * 
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
        HashMap<Field, IntField> results = null;
        switch (this.operator) {
        case MIN:
        case MAX:
        case SUM:
        case COUNT:
            results = this.aggregates;
            break;
        case AVG:
            results = new HashMap<Field, IntField>();
            for (Field f : this.aggregates.keySet()) {
                int average = this.aggregates.get(f).getValue() /
                              this.counts.get(f);
                results.put(f, new IntField(average));
            }
            break;
        default:
            throw new AssertionError(String.format(
                    "Operator not supported %s", this.operator));
        }
        
        boolean wantsNoGrouping = (this.groupByFieldIndex == NO_GROUPING);
        return new IntegerAggregatorTupleIterator(
                results, this.groupByFieldType, wantsNoGrouping);
    }
    
    private class IntegerAggregatorTupleIterator implements DbIterator {
        private static final long serialVersionUID = 1L;
        
        private final HashMap<Field, IntField> results;
        private final Type groupByFieldType;
        
        private boolean isOpen;
        private Iterator<Map.Entry<Field, IntField>> resultsIterator;
        private final boolean wantsNoGrouping;
        
        public IntegerAggregatorTupleIterator(HashMap<Field, IntField> results,
                                              Type groupByFieldType,
                                              boolean wantsNoGrouping) {
            this.results = results;
            this.groupByFieldType = groupByFieldType;
            
            this.isOpen = false;
            this.resultsIterator = this.results.entrySet().iterator();
            this.wantsNoGrouping = wantsNoGrouping;
        }

        @Override
        public void open() throws DbException, TransactionAbortedException {
            this.isOpen = true;
        }

        @Override
        public boolean hasNext() throws DbException,
                TransactionAbortedException {
            if (!this.isOpen) {
                throw new DbException("open() not called on iterator.");
            }
            return this.resultsIterator.hasNext();
        }

        @Override
        public Tuple next() throws DbException, TransactionAbortedException,
                NoSuchElementException {
            if (!this.isOpen) {
                throw new DbException("open() not called on iterator.");
            }
            Tuple tuple = new Tuple(this.getTupleDesc());
            Map.Entry<Field, IntField> entry = this.resultsIterator.next();
            
            if (this.wantsNoGrouping) {
                tuple.setField(0, entry.getValue());
            } else {
                tuple.setField(0, entry.getKey());
                tuple.setField(1, entry.getValue());
            }
            return tuple;
        }

        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            this.resultsIterator = this.results.entrySet().iterator();
        }

        @Override
        public TupleDesc getTupleDesc() {
            if (this.wantsNoGrouping) {
                return new TupleDesc(new Type[]{Type.INT_TYPE});
            }
            
            return new TupleDesc(new Type[]{
                    this.groupByFieldType,
                    Type.INT_TYPE
            });
        }

        @Override
        public void close() {
            this.isOpen = false;
        }
    }

}
