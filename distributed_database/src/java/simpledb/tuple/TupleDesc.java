package simpledb.tuple;

import java.io.Serializable;
import java.util.*;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc implements Serializable {

    /**
     * A help class to facilitate organizing the information of each field
     * */
    public static class TDItem implements Serializable {

        private static final long serialVersionUID = 1L;

        /**
         * The type of the field
         * */
        public final Type fieldType;
        
        /**
         * The name of the field
         * */
        public final String fieldName;

        public TDItem(Type t, String n) {
            this.fieldName = n;
            this.fieldType = t;
        }

        public String toString() {
            return fieldName + "(" + fieldType + ")";
        }
        
        public int hashCode() {
            int typeHash = 0;
            switch (this.fieldType) {
                case INT_TYPE:
                    typeHash = 1;
                    break;
                case STRING_TYPE:
                    typeHash = 2;
                    break;
                default:
                    throw new AssertionError("Unsupported field type: " + this.fieldType.toString());
            }
            return (typeHash << 16) + this.fieldName.hashCode();
        }
    }
    
    private final TDItem[] tupleDescriptorItems;

    /**
     * @return
     *        An iterator which iterates over all the field TDItems
     *        that are included in this TupleDesc
     * */
    public Iterator<TDItem> iterator() {
        return Arrays.asList(this.tupleDescriptorItems).iterator();
    }

    private static final long serialVersionUID = 1L;

    /**
     * Create a new TupleDesc with typeAr.length fields with fields of the
     * specified types, with associated named fields.
     * 
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     * @param fieldAr
     *            array specifying the names of the fields. Note that names may
     *            be null.
     */
    public TupleDesc(Type[] typeAr, String[] fieldAr) {
        if (typeAr.length != fieldAr.length) {
            throw new AssertionError("Type array and field names array " +
                                     "don\'t have the same length.");
        }
        this.tupleDescriptorItems = new TDItem[typeAr.length];
        for (int i = 0; i < this.tupleDescriptorItems.length; i++) {
            this.tupleDescriptorItems[i] = new TDItem(typeAr[i], fieldAr[i]);
        }
    }

    /**
     * Constructor. Create a new tuple desc with typeAr.length fields with
     * fields of the specified types, with anonymous (unnamed) fields.
     * 
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     */
    public TupleDesc(Type[] typeAr) {
        this(typeAr, new String[typeAr.length]);
    }

    /**
     * @return the number of fields in this TupleDesc
     */
    public int numFields() {
        return this.tupleDescriptorItems.length;
    }

    /**
     * Gets the (possibly null) field name of the ith field of this TupleDesc.
     * 
     * @param i
     *            index of the field name to return. It must be a valid index.
     * @return the name of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public String getFieldName(int i) throws NoSuchElementException {
        if (0 <= i && i < this.tupleDescriptorItems.length) {
            return this.tupleDescriptorItems[i].fieldName;
        }
        throw new NoSuchElementException(
                "Index " + i + " does not exist. " +
                "There are only " + this.tupleDescriptorItems.length +
                " fields.");
    }

    /**
     * Gets the type of the ith field of this TupleDesc.
     * 
     * @param i
     *            The index of the field to get the type of. It must be a valid
     *            index.
     * @return the type of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public Type getFieldType(int i) throws NoSuchElementException {
        if (0 <= i && i < this.tupleDescriptorItems.length) {
            return this.tupleDescriptorItems[i].fieldType;
        }
        throw new NoSuchElementException(
                "Index " + i + " does not exist. " +
                "There are only " + this.tupleDescriptorItems.length +
                " fields.");
    }

    /**
     * Find the index of the field with a given name.
     * 
     * @param name
     *            name of the field.
     * @return the index of the field that is first to have the given name.
     * @throws NoSuchElementException
     *             if no field with a matching name is found.
     */
    public int fieldNameToIndex(String name) throws NoSuchElementException {
        for (int i = 0; i < this.tupleDescriptorItems.length; i++) {
            if (this.tupleDescriptorItems[i].fieldName != null &&
                    this.tupleDescriptorItems[i].fieldName.equals(name)) {
                return i;
            }
        }
        throw new NoSuchElementException(
                "Field name " + name + " does not exist.");
    }

    /**
     * @return The size (in bytes) of tuples corresponding to this TupleDesc.
     *         Note that tuples from a given TupleDesc are of a fixed size.
     */
    public int getSize() {
        int totalSize = 0;
        for (TDItem i : this.tupleDescriptorItems) {
            totalSize += i.fieldType.getLen();
        }
        return totalSize;
    }

    /**
     * Merge two TupleDescs into one, with td1.numFields + td2.numFields fields,
     * with the first td1.numFields coming from td1 and the remaining from td2.
     * 
     * @param td1
     *            The TupleDesc with the first fields of the new TupleDesc
     * @param td2
     *            The TupleDesc with the last fields of the TupleDesc
     * @return the new TupleDesc
     */
    public static TupleDesc merge(TupleDesc td1, TupleDesc td2) {
        Type[] types = new Type[td1.numFields() + td2.numFields()];
        String[] names = new String[td1.numFields() + td2.numFields()];
        
        int counter = 0;
        // Add in the names and fields of the first TupleDesc.
        for (int i = 0; i < td1.numFields(); i++) {
            types[counter] = td1.getFieldType(i);
            names[counter] = td1.getFieldName(i);
            counter++;
        }
        // Add in the names and fields of the second TupleDesc.
        for (int i = 0; i < td2.numFields(); i++) {
            types[counter] = td2.getFieldType(i);
            names[counter] = td2.getFieldName(i);
            counter++;
        }
        // Assertion check
        if (counter != td1.numFields() + td2.numFields()) {
            throw new AssertionError("Did not iterate through all fields.");
        }
        return new TupleDesc(types, names);
    }

    /**
     * Compares the specified object with this TupleDesc for equality. Two
     * TupleDescs are considered equal if they are the same size and if the n-th
     * type in this TupleDesc is equal to the n-th type in td.
     * 
     * @param o
     *            the Object to be compared for equality with this TupleDesc.
     * @return true if the object is equal to this TupleDesc.
     */
    public boolean equals(Object o) {
        // Check to make sure the objects are both TupleDesc's.
        if (!(o instanceof TupleDesc)) {
            return false;
        }
        
        TupleDesc tupleDesc = (TupleDesc)(o);
        
        // Check to make sure there are the same number of fields.
        if (this.tupleDescriptorItems.length != tupleDesc.numFields()) {
            return false;
        }
        
        // Check to make sure the fields match.
        for (int i = 0; i < this.numFields(); i++) {
            if (this.tupleDescriptorItems[i].fieldType !=
                    tupleDesc.getFieldType(i)) {
                return false;
            }
        }

        return true;
    }

    public int hashCode() {
        int value = 0;
        for (int i = 0; i < this.tupleDescriptorItems.length; i++) {
            value += (i + 1) * this.tupleDescriptorItems[i].hashCode();
        }
        return value;
    }

    /**
     * Returns a String describing this descriptor. It should be of the form
     * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
     * the exact format does not matter.
     * 
     * @return String describing this descriptor.
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < this.tupleDescriptorItems.length; i++) {
            if (i > 0) {
                builder.append(", ");
            }
            builder.append(this.tupleDescriptorItems[i].toString());
        }
        return builder.toString();
    }
}
