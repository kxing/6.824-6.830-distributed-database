package simpledb.tuple;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;

import simpledb.infrastructure.RecordId;

/**
 * Tuple maintains information about the contents of a tuple. Tuples have a
 * specified schema specified by a TupleDesc object and contain Field objects
 * with the data for each field.
 */
public class Tuple implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private TupleDesc tupleDesc;
    private Field[] fields;
    private RecordId recordId;

    /**
     * Create a new tuple with the specified schema (type).
     * 
     * @param td
     *            the schema of this tuple. It must be a valid TupleDesc
     *            instance with at least one field.
     */
    public Tuple(TupleDesc td) {
        this.resetTupleDesc(td);
    }

    /**
     * @return The TupleDesc representing the schema of this tuple.
     */
    public TupleDesc getTupleDesc() {
        return this.tupleDesc;
    }

    /**
     * @return The RecordId representing the location of this tuple on disk. May
     *         be null.
     */
    public RecordId getRecordId() {
        return this.recordId;
    }

    /**
     * Set the RecordId information for this tuple.
     * 
     * @param rid
     *            the new RecordId for this tuple.
     */
    public void setRecordId(RecordId rid) {
        this.recordId = rid;
    }

    /**
     * Change the value of the ith field of this tuple.
     * 
     * @param i
     *            index of the field to change. It must be a valid index.
     * @param f
     *            new value for the field.
     */
    public void setField(int i, Field f) {
        // Check that the field types match.
        if (this.tupleDesc.getFieldType(i) != f.getType()) {
            throw new AssertionError(
                    "Expected field type: " +
                    this.tupleDesc.getFieldType(i).toString() +
                    " but got field type: " + f.getType());
        }

        this.fields[i] = f;
    }

    /**
     * @return the value of the ith field, or null if it has not been set.
     * 
     * @param i
     *            field index to return. Must be a valid index.
     */
    public Field getField(int i) {
        return this.fields[i];
    }

    /**
     * Returns the contents of this Tuple as a string. Note that to pass the
     * system tests, the format needs to be as follows:
     * 
     * column1\tcolumn2\tcolumn3\t...\tcolumnN\n
     * 
     * where \t is any whitespace, except newline, and \n is a newline
     */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < this.tupleDesc.numFields(); i++) {
            builder.append(this.fields[i].toString());
            if (i < this.tupleDesc.numFields() - 1) {
                builder.append('\t');
            } else {
                builder.append('\n');
            }
        }
        return builder.toString();
    }
    
    /**
     * @return
     *        An iterator which iterates over all the fields of this tuple
     * */
    public Iterator<Field> fields() {
        return Arrays.asList(this.fields).iterator();
    }
    
    /**
     * reset the TupleDesc of this tuple
     * */
    public void resetTupleDesc(TupleDesc td) {
        this.tupleDesc = td;
        this.fields = new Field[this.tupleDesc.numFields()];
        if (this.tupleDesc.numFields() == 0) {
            throw new AssertionError("TupleDesc has no fields.");
        }
    }
}
