package simpledb.infrastructure;

import java.io.*;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import simpledb.transaction.Lock;
import simpledb.transaction.Permissions;
import simpledb.transaction.TransactionAbortedException;
import simpledb.transaction.TransactionId;
import simpledb.tuple.Tuple;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 * 
 * @Threadsafe, all fields are final
 */
public class BufferPool {
    /** Bytes per page, including header. */
    private static final int PAGE_SIZE = 4096;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;
    
    private final ConcurrentHashMap<PageId, Page> pages;
    private final ConcurrentHashMap<PageId, Lock> locks;
    private final int maxPages;
    
    private final int BUFFER_POOL_PAGE_LOCK_GRANULARITY;
    private final Object[] bufferPoolPageLocks;

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        this.pages = new ConcurrentHashMap<PageId, Page>();
        this.locks = new ConcurrentHashMap<PageId, Lock>();
        this.maxPages = numPages;
        
        // Initialize the fine-grained locks for pages in our buffer pool.
        this.BUFFER_POOL_PAGE_LOCK_GRANULARITY = numPages * 2;
        this.bufferPoolPageLocks =
                new Object[this.BUFFER_POOL_PAGE_LOCK_GRANULARITY];
        for (int i = 0; i < this.BUFFER_POOL_PAGE_LOCK_GRANULARITY; i++) {
            this.bufferPoolPageLocks[i] = new Object();
        }
    }
    
    public static int getPageSize() {
      return PAGE_SIZE;
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
        throws TransactionAbortedException, DbException {

        // We need to lock the state for the page pool, so that we don't do
        // stupid things like evict two pages when two transactions find that
        // a page is not in the buffer.
        int chosenIndex = ((pid.hashCode() %
                this.BUFFER_POOL_PAGE_LOCK_GRANULARITY) +
                this.BUFFER_POOL_PAGE_LOCK_GRANULARITY) %
                this.BUFFER_POOL_PAGE_LOCK_GRANULARITY;
        
        synchronized (this.bufferPoolPageLocks[chosenIndex]) {
            // Read in the page, if it isn't in the buffer pool already.
            if (!this.pages.containsKey(pid)) {
                synchronized (this) {
                    // Evict a page, if we have to.
                    if (this.pages.size() >= this.maxPages) {
                        this.evictPage();
                    }
                    int tableId = pid.getTableId();
                    Page page = Database.getCatalog().getDatabaseFile(tableId)
                            .readPage(pid);
                
                    this.pages.put(pid, page);
                    if (!this.locks.containsKey(pid)) {
                        this.locks.put(pid, new Lock());
                    }
                }
            }
        }
        
        // Grab the appropriate lock.
        try {
            this.locks.get(pid).acquireLock(tid, perm);
        } catch (TransactionAbortedException e) {
            try {
                // Abort if we have a TransactionAbortedException.
                this.transactionComplete(tid, false);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
            throw e;
        }
        
        return this.pages.get(pid);
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public void releasePage(TransactionId tid, PageId pid) {
        this.locks.get(pid).releaseLock(tid);
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        this.transactionComplete(tid, true);
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(TransactionId tid, PageId p) {
        return this.locks.get(p).holdsLock(tid);
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
        
        if (commit) {
            // Commit -> write the pages to disk.
            // TODO(kxing): Make this more efficient.
            for (Map.Entry<PageId, Page> entry : this.pages.entrySet()) {
                PageId pageId = entry.getKey();
                Page page = entry.getValue();
                TransactionId owner = page.isDirty();
                
                if (owner != null && owner.equals(tid)) {
                    this.flushPage(pageId);
                }
            }
        } else {
            // Abort -> Figure out the pages we need to read back from disk.
            ArrayList<PageId> rereadPageIds = new ArrayList<PageId>();
            for (Map.Entry<PageId, Page> entry : this.pages.entrySet()) {
                PageId pageId = entry.getKey();
                Page page = entry.getValue();
                TransactionId owner = page.isDirty();
                
                if (owner != null && owner.equals(tid)) {
                    rereadPageIds.add(pageId);
                }
            }
            
            // Update the buffer pool.
            for (PageId pageId : rereadPageIds) {
                int tableId = pageId.getTableId();
                Page newPage = Database.getCatalog().getDatabaseFile(tableId)
                                .readPage(pageId);
                this.pages.put(pageId, newPage);
            }
        }
        
        // Release all the locks.
        // TODO(kxing): Make this more efficient.
        for (Lock lock : this.locks.values()) {
            if (lock.holdsLock(tid)) {
                lock.releaseLock(tid);
            }
        }
    }

    /**
     * Add a tuple to the specified table behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to(Lock 
     * acquisition is not needed for lab2). May block if the lock cannot 
     * be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and updates cached versions of any pages that have 
     * been dirtied so that future requests see up-to-date pages. 
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        
        // Call insertTuple() on the DbFile associated with the table.
        ArrayList<Page> dirtyPages =
            Database.getCatalog().getDatabaseFile(tableId).insertTuple(tid, t);
        // Mark all the modified pages as dirty.
        for (Page p : dirtyPages) {
            p.markDirty(true, tid);
        }
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from. May block if
     * the lock cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit.  Does not need to update cached versions of any pages that have 
     * been dirtied, as it is not possible that a new page was created during the deletion
     * (note difference from addTuple).
     *
     * @param tid the transaction deleting the tuple.
     * @param t the tuple to delete
     */
    public  void deleteTuple(TransactionId tid, Tuple t)
        throws DbException, TransactionAbortedException {

        // Remove the tuple from the appropriate DbFile.
        int tableId = t.getRecordId().getPageId().getTableId();
        Page dirtyPage =
            Database.getCatalog().getDatabaseFile(tableId).deleteTuple(tid, t);
        
        // Mark the page as dirty.
        dirtyPage.markDirty(true, tid);
    }

    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        for (PageId pageId : this.pages.keySet()) {
            // Flush the page to disk.
            this.flushPage(pageId);
        }

    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.
    */
    public synchronized void discardPage(PageId pid) {
        // some code goes here
        // only necessary for lab5
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     */
    private synchronized void flushPage(PageId pid) throws IOException {
        Page flushedPage = this.pages.get(pid);
        
        // Write to disk if the page is dirty.
        if (flushedPage.isDirty() != null) {
            int tableId = pid.getTableId();
            
            // Actually write to disk.
            Database.getCatalog().getDatabaseFile(tableId).writePage(flushedPage);
            
            // Mark the page as not dirty.
            flushedPage.markDirty(false, null);
        }
    }

    /** Write all pages of the specified transaction to disk.
     */
    public synchronized void flushPages(TransactionId tid) throws IOException {
        for (PageId pageId : this.pages.keySet()) {
            if (this.locks.get(pageId).holdsLock(tid)) {
                this.flushPage(pageId);
            }
        }
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized void evictPage() throws DbException {
        PageId pageIdToEvict = null;
        for (Map.Entry<PageId, Page> entry : this.pages.entrySet()) {
            PageId pageId = entry.getKey();
            Page candidatePage = entry.getValue();
            if (candidatePage.isDirty() == null) {
                pageIdToEvict = pageId;
                break;
            }
        }
        
        if (pageIdToEvict != null) {
            this.pages.remove(pageIdToEvict);
            return;
        }
        
        throw new DbException("Cannot evict any pages: All pages are dirty.");
    }

}
