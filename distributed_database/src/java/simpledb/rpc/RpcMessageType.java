package simpledb.rpc;

public abstract class RpcMessageType {

    public abstract String getRpcMessage();
}
