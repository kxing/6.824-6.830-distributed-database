package simpledb.rpc;

public abstract class RpcResponseType {

    public abstract String getRpcReply();
}
