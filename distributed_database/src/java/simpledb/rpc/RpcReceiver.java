package simpledb.rpc;

import java.io.IOException;
import java.net.Socket;

import simpledb.network.NetworkConnection;
import simpledb.paxos.PaxosStatics;
import simpledb.paxos.rpc.PaxosAcceptMessage;
import simpledb.paxos.rpc.PaxosAcceptReply;
import simpledb.paxos.rpc.PaxosDecidedMessage;
import simpledb.paxos.rpc.PaxosDecidedReply;
import simpledb.paxos.rpc.PaxosPrepareMessage;
import simpledb.paxos.rpc.PaxosPrepareReply;

public class RpcReceiver implements Runnable {
    private final PaxosStatics statics;
    private final Socket clientSocket;

    private RpcReceiver(PaxosStatics statics, Socket clientSocket) {
        this.statics = statics;
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try {
            NetworkConnection connection =
                new NetworkConnection(this.clientSocket);
            
            // Read in the RPC call that the client wanted to make.
            String rpcCall = connection.receiveData();
            
            String response = this.getRpcResponse(rpcCall);
            
            // Write out the response to the RPC call.
            connection.sendData(response);
            
            // Shut down the connection.
            connection.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    private String getRpcResponse(String rpcCall) {
        // TODO(kxing): Add the implementations of RPC calls here.
        if (PaxosPrepareMessage.matchesRpcMessage(rpcCall)) {
            // Parse the RPC message.
            PaxosPrepareMessage prepareMessage =
                    PaxosPrepareMessage.parseMessage(rpcCall);
            
            // Compute the reply.
            PaxosPrepareReply prepareReply =
                    this.statics.getPaxos().handlePrepareRpc(prepareMessage);
            
            // Return the reply as a String.
            return prepareReply.getRpcReply();
        } else if (PaxosAcceptMessage.matchesRpcMessage(rpcCall)) {
            // Parse the RPC message.
            PaxosAcceptMessage acceptMessage =
                    PaxosAcceptMessage.parseMessage(rpcCall);
            
            // Compute the reply.
            PaxosAcceptReply acceptReply =
                    this.statics.getPaxos().handleAcceptRpc(acceptMessage);
            
            // Return the reply as a String.
            return acceptReply.getRpcReply();
        } else if (PaxosDecidedMessage.matchesRpcMessage(rpcCall)) {
            // Parses the RPC message.
            PaxosDecidedMessage decidedMessage =
                    PaxosDecidedMessage.parseMessage(rpcCall);
            
            // Compute the reply.
            PaxosDecidedReply decidedReply =
                    this.statics.getPaxos().handleDecidedRpc(decidedMessage);
            
            // Return the reply as a String.
            return decidedReply.getRpcReply();
        }
        throw new AssertionError("Can\'t handle RPC: " + rpcCall);
    }
    
    public static void handleClient(PaxosStatics statics,
                                    Socket clientSocket) {
        (new Thread(new RpcReceiver(statics, clientSocket))).start();
    }
}
