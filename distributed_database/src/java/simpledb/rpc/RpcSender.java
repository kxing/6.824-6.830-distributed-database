package simpledb.rpc;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import simpledb.network.NetworkAddressPort;
import simpledb.network.NetworkConnection;
import simpledb.paxos.PaxosConfiguration;
import simpledb.paxos.PaxosStatics;
import simpledb.paxos.rpc.PaxosAcceptMessage;
import simpledb.paxos.rpc.PaxosAcceptReply;
import simpledb.paxos.rpc.PaxosDecidedMessage;
import simpledb.paxos.rpc.PaxosDecidedReply;
import simpledb.paxos.rpc.PaxosPrepareMessage;
import simpledb.paxos.rpc.PaxosPrepareReply;

public class RpcSender {
    
    private static final int SOCKET_TIMEOUT = 100;
    
    private static String makeRpc(NetworkAddressPort addressPort, String rpc)
            throws UnknownHostException, IOException {
        Socket socket = new Socket(addressPort.getAddress(),
                                   addressPort.getPort());
        socket.setSoTimeout(SOCKET_TIMEOUT);
        
        NetworkConnection connection = new NetworkConnection(socket);
        
        // Make the RPC call to the server.
        connection.sendData(rpc);
        
        // Retrieve the response.
        String response = connection.receiveData();
        
        // Close the connection.
        connection.shutdown();
        
        return response;
    }
    
    // TODO(kxing): Make separate functions for each type of RPC.
    
    public static PaxosPrepareReply makePaxosPrepareRpc(
            NetworkAddressPort addressPort, PaxosPrepareMessage prepareMessage)
            throws IOException {
        String reply =
                RpcSender.makeRpc(addressPort, prepareMessage.getRpcMessage());
        return PaxosPrepareReply.parseReply(reply);
    }
    
    public static PaxosAcceptReply makePaxosAcceptRpc(
            NetworkAddressPort addressPort, PaxosAcceptMessage acceptMessage)
            throws IOException {
        String reply =
                RpcSender.makeRpc(addressPort, acceptMessage.getRpcMessage());
        return PaxosAcceptReply.parseReply(reply);
    }
    
    public static PaxosDecidedReply makePaxosDecidedRpc(
            NetworkAddressPort addressPort, PaxosDecidedMessage decidedMessage)
            throws IOException {
        String reply =
                RpcSender.makeRpc(addressPort, decidedMessage.getRpcMessage());
        return PaxosDecidedReply.parseReply(reply);
        
    }
    
    // TODO(kxing): Remove this test code.
    public static void main(String[] args) throws IOException {
        PaxosConfiguration config = new PaxosConfiguration("solo_server.config");
        PaxosStatics statics = new PaxosStatics(config);
        statics.startRpcListener();
        
        for (int i = 0; i < 10; i++) {
            PaxosPrepareReply reply = RpcSender.makePaxosPrepareRpc(new NetworkAddressPort("localhost", config.getPort()), new PaxosPrepareMessage(1, 2));
            System.out.println(reply.getRpcReply());
//            PaxosAcceptReply reply = RpcSender.makePaxosAcceptRpc(new NetworkAddressPort("localhost", config.getPort()), new PaxosAcceptMessage(1, 2, "Hi"));
//            System.out.println(reply.getRpcReply());
//            PaxosDecidedReply reply = RpcSender.makePaxosDecidedRpc(new NetworkAddressPort("localhost", config.getPort()), new PaxosDecidedMessage(1, "hi"));
//            System.out.println("Stuff: "+ reply.getRpcReply());
        }
    }
}
