package simpledb.rpc;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import simpledb.paxos.PaxosStatics;

public class RpcListener implements Runnable {
    private final PaxosStatics statics;
    private final ServerSocket serverSocket;
    
    private static final int SOCKET_TIMEOUT = 100;
    
    private RpcListener(PaxosStatics statics, ServerSocket serverSocket) {
        this.statics = statics;
        this.serverSocket = serverSocket;
    }

    @Override
    public void run() {
        while (this.statics.isRunning()) {
            try {
                this.serverSocket.setSoTimeout(SOCKET_TIMEOUT);
                Socket clientSocket = this.serverSocket.accept();
                
                // Handle the client on a new thread.
                RpcReceiver.handleClient(statics, clientSocket);
            } catch (SocketTimeoutException e) {
                // This is okay.
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Starts the {@link RpcListener} by spawning a new thread to listen to the
     * port.
     * 
     * @param port The port number to use.
     */
    public static void startRpcListener(PaxosStatics statics, int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            (new Thread(new RpcListener(statics, serverSocket))).start();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
