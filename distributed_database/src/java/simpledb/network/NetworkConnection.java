package simpledb.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class NetworkConnection {
    private final Socket socket;
    private final BufferedReader inputStream;
    private final PrintWriter outputStream;
    
    public NetworkConnection(Socket socket) throws IOException {
        this.socket = socket;
        this.inputStream = new BufferedReader(
                new InputStreamReader(this.socket.getInputStream()));
        this.outputStream = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(this.socket.getOutputStream())));
    }
    
    public void sendData(String data) {
        this.outputStream.println(data);
        this.outputStream.flush();
    }
    
    public String receiveData() throws IOException {
        return this.inputStream.readLine();
    }
    
    public void shutdown() throws IOException {
        this.inputStream.close();
        this.outputStream.close();
        this.socket.close();
    }
    
}
