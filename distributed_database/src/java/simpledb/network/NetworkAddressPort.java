package simpledb.network;

/**
 * Immutable network address and port structure.
 *
 * @author kxing
 */
public class NetworkAddressPort {
    private final String address;
    private final int port;

    /**
     * Creates a new {@link NetworkAddressPort} object. Throws a
     * {@link NetworkException} if the IP address is obviously invalid.
     * 
     * @param address An IP address of the form xx.xx.xx.xx or "localhost".
     * @param port The port number to use.
     */
    public NetworkAddressPort(String address, int port) {
        boolean validAddress = true;
        if (address.equals("localhost")) {
            // The actual string "localhost".
            validAddress = true;
        } else if (!address.matches("\\d+\\.\\d+\\.\\d+\\.\\d+")) {
            // Some address that is not of the form "xx.xx.xx.xx".
            validAddress = false;
        } else {
            // Some address of the form "xx.xx.xx.xx".
            String[] numbers = address.split("\\.");
            if (numbers.length != 4) {
                validAddress = false;
            } else {
                for (int i = 0; i < numbers.length; i++) {
                    int number = Integer.parseInt(numbers[i]);
                    if (number < 0 || number >= 256) {
                        validAddress = false;
                    }
                }
            }
        }
        
        if (!validAddress) {
            throw new NetworkException(
                    String.format("Invalid address: %s", address));
        }
        
        this.address = address;
        this.port = port;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public int getPort() {
        return this.port;
    }
    
    public int hashCode() {
        return String.format("%s:%d", this.address, this.port).hashCode();
    }
}
