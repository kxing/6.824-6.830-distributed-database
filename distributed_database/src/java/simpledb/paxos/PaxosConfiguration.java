package simpledb.paxos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import simpledb.network.NetworkAddressPort;

public class PaxosConfiguration {
    private final String fileName;
    
    private String paxosLogFileName;
    private String paxosLogNumberFileName;
    private String paxosInstanceStateFileName;
    private NetworkAddressPort[] otherServers;
    private int index;
    private String address;
    private int port;

    public PaxosConfiguration(String fileName) throws IOException {
        this.fileName = fileName;
        
        this.readInConfiguration();
    }
    
    public String getPaxosLogFileName() {
        return this.paxosLogFileName;
    }
    
    public String getPaxosLogNumberFileName() {
        return this.paxosLogNumberFileName;
    }
    
    public String getPaxosInstanceStateFileName() {
        return this.paxosInstanceStateFileName;
    }
    
    public NetworkAddressPort[] getOtherServers() {
        return this.otherServers;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public int getPort() {
        return this.port;
    }
    
    private void readInConfiguration() throws IOException {
        BufferedReader r = new BufferedReader(new FileReader(this.fileName));
        
        this.paxosLogFileName = r.readLine();
        this.paxosLogNumberFileName = r.readLine();
        this.paxosInstanceStateFileName = r.readLine();
        
        int numberOfServers = Integer.parseInt(r.readLine());
        if (numberOfServers < 1) {
            throw new PaxosException("Invalid config file: " +
                                     this.fileName);
        }
        this.index = Integer.parseInt(r.readLine());
        
        this.otherServers = new NetworkAddressPort[numberOfServers - 1];
        
        int counter = 0;
        for (int i = 0; i < numberOfServers; i++) {
            String line = r.readLine();
            
            String[] tokens = line.split(" ");
            if (tokens.length != 2) {
                throw new PaxosException("Invalid config file: " +
                                         this.fileName);
            }
            
            String address = tokens[0];
            int port = Integer.parseInt(tokens[1]);

            if (i == this.index) {
                // Our own server.
                this.address = address;
                this.port = port;
            } else {
                // One of the other servers.
                this.otherServers[counter] = new NetworkAddressPort(address, port);
                counter++;
            }
        }
        
        if (counter != this.otherServers.length) {
            throw new AssertionError();
        }
    }
    
    public static void main(String[] args) throws IOException {
        PaxosConfiguration pc = new PaxosConfiguration("server2.config");
        
        for (NetworkAddressPort otherServer : pc.getOtherServers()) {
            System.out.println(String.format("%s:%d", otherServer.getAddress(), otherServer.getPort()));
        }
        System.out.println(pc.getIndex());
    }
}
