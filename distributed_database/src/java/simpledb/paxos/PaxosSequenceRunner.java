package simpledb.paxos;

import java.util.Random;

/**
 * A dedicated runner to get an agreement on a particular sequence number.
 * 
 * @author kxing
 */
public class PaxosSequenceRunner implements Runnable {
    private final Paxos paxos;
    private final int sequenceNumber;
    private final String value;
    private final Random random;
    
    public PaxosSequenceRunner(Paxos paxos, int sequenceNumber, String value) {
        this.paxos = paxos;
        this.sequenceNumber = sequenceNumber;
        this.value = value;
        this.random = new Random();
    }
    
    public void run() {
        // Start a new thread to start the Paxos proposing.
        int proposalNumber = this.paxos.getIndex();
        boolean keepGoing = true;
        
        while (keepGoing) {
            keepGoing = !propose(sequenceNumber, proposalNumber, value);
            
            // Increase the proposal number for next time.
            proposalNumber += (this.paxos.getOtherPaxosServers().length + 1);
            
            try {
                // Sleep between 10ms and 100ms, for jitter.
                Thread.sleep(10 * (this.random.nextInt(10) + 1));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            
            // Check if the Paxos log contains the item of interest.
            if (this.paxos.containsSequenceNumber(sequenceNumber)) {
                keepGoing = false;
            }
        }
    }
    
    /**
     * Proposes, using the given sequence number, proposal number, and value.
     * Returns whether the proposition was successful.
     * 
     * @param sequenceNumber The sequence number to use.
     * @param proposalNumber The proposal number to use.
     * @param value The value to use.
     * @return Whether the proposition was successful.
     */
    private boolean propose(int sequenceNumber, int proposalNumber,
                           String value) {
        // Prepare phase.
        PaxosPreparePhase preparePhase =
                PaxosPreparePhase.doPreparePhase(this.paxos, sequenceNumber,
                                                 proposalNumber, value);
        
        if (!preparePhase.isSuccess()) {
            return false;
        }
        
        // Accept phase.
        String chosenProposalValue = preparePhase.getBestProposalValue();
        
        PaxosAcceptPhase acceptPhase = PaxosAcceptPhase.doAcceptPhase(
                this.paxos,
                sequenceNumber,
                proposalNumber,
                chosenProposalValue);
        
        if (!acceptPhase.isSuccess()) {
            return false;
        }
        
        // Decided phase.
        PaxosDecidedPhase.doDecidedPhase(this.paxos,
                                         sequenceNumber,
                                         chosenProposalValue);
        
        return true;
    }
}
