package simpledb.paxos.rpc;

import simpledb.paxos.PaxosException;
import simpledb.rpc.RpcMessageType;

public class PaxosPrepareMessage extends RpcMessageType {
    private static final String RPC_NAME = "PaxosPrepare";
    
    private final int sequenceNumber;
    private final int proposalNumber;

    public PaxosPrepareMessage(int sequenceNumber, int proposalNumber) {
        this.sequenceNumber = sequenceNumber;
        this.proposalNumber = proposalNumber;
    }
    
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }
    
    public int getProposalNumber() {
        return this.proposalNumber;
    }
    
    public String getRpcMessage() {
        return String.format("%s %d %d", RPC_NAME,
                                         this.sequenceNumber,
                                         this.proposalNumber);
    }
    
    /**
     * Checks whether the RPC message is a {@link PaxosPrepareMessage}.
     * 
     * @param rpcMessage The RPC message received.
     * @return Whether the RPC message is a {@link PaxosPrepareMessage}.
     */
    public static boolean matchesRpcMessage(String rpcMessage) {
        String regex = String.format("%s \\d+ \\d+", RPC_NAME);
        return rpcMessage.matches(regex);
    }
    
    /**
     * Converts the RPC message into a {@link PaxosPrepareMessage}.
     * 
     * @param rpcMessage The RPC message received.
     * @return The {@link PaxosPrepareMessage} received.
     */
    public static PaxosPrepareMessage parseMessage(String rpcMessage) {
        if (!PaxosPrepareMessage.matchesRpcMessage(rpcMessage)) {
            throw new PaxosException(
                    "RPC call is not a PrepareMessage: " + rpcMessage);
        }
        
        String[] tokens = rpcMessage.split(" ");
        if (tokens.length != 3) {
            throw new PaxosException(
                    "RPC call is not a PrepareMessage: " + rpcMessage);
        }
        
        int sequenceNumber = Integer.parseInt(tokens[1]);
        int proposalNumber = Integer.parseInt(tokens[2]);
        
        return new PaxosPrepareMessage(sequenceNumber, proposalNumber);
    }
}
