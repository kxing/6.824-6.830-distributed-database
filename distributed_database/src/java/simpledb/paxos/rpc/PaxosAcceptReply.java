package simpledb.paxos.rpc;

import simpledb.paxos.PaxosException;
import simpledb.rpc.RpcResponseType;

public class PaxosAcceptReply extends RpcResponseType {
    private final boolean ok;
    
    // Piggybacking the done value.
    private final int doneValue;
    
    public PaxosAcceptReply(boolean ok, int doneValue) {
        this.ok = ok;
        
        this.doneValue = doneValue;
    }
    
    public boolean isOk() {
        return this.ok;
    }
    
    public int getDoneValue() {
        return this.doneValue;
    }

    public String getRpcReply() {
        return String.format("%s %d", Boolean.toString(this.ok), this.doneValue);
    }
    
    /**
     * Converts a String representation of a {@link PaxosAcceptReply} into a
     * {@link PaxosAcceptReply}.
     * 
     * @param reply A String representation of the {@link PaxosAcceptReply}.
     * @return The {@link PaxosAcceptReply} represented by the String.
     */
    public static PaxosAcceptReply parseReply(String reply) {
        if (!reply.matches("(true|false) \\-?\\d+")) {
            throw new PaxosException("Invalid PaxosPrepareReply: " + reply);
        }
        
        String[] tokens = reply.split(" ");
        if (tokens.length != 2) {
            throw new PaxosException("Invalid PaxosPrepareReply: " + reply);
        }
        
        boolean ok = Boolean.parseBoolean(tokens[0]);
        int doneValue = Integer.parseInt(tokens[1]);
        
        return new PaxosAcceptReply(ok, doneValue);
    }
}
