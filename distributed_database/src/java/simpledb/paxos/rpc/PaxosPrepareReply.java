package simpledb.paxos.rpc;

import simpledb.paxos.PaxosException;
import simpledb.rpc.RpcResponseType;

public class PaxosPrepareReply extends RpcResponseType {
    private final boolean ok;
    private final int highestAcceptNumber;
    private final String highestAcceptValue;
    
    // Piggy-backed done value;
    private final int doneValue;

    public PaxosPrepareReply(boolean ok,
                             int highestAcceptNumber,
                             String highestAcceptValue,
                             int doneValue) {
        this.ok = ok;
        this.highestAcceptNumber = highestAcceptNumber;
        this.highestAcceptValue = highestAcceptValue;
        
        this.doneValue = doneValue;
    }
    
    public boolean isOk() {
        return this.ok;
    }
    
    public int getHighestAcceptNumber() {
        return this.highestAcceptNumber;
    }
    
    public String getHighestAcceptValue() {
        return this.highestAcceptValue;
    }
    
    public int getDoneValue() {
        return this.doneValue;
    }
    
    /**
     * Converts the {@link PaxosPrepareReply} into a String, to be sent across
     * the network.
     * 
     * @return A string representation of the {@link PaxosPrepareReply}.
     */
    public String getRpcReply() {
        return String.format("%s %d %d %s", Boolean.toString(this.ok),
                                            this.doneValue,
                                            this.highestAcceptNumber,
                                            this.highestAcceptValue);
    }
    
    /**
     * Converts a String representation of a {@link PaxosPrepareReply} into a
     * {@link PaxosPrepareReply}.
     * 
     * @param reply A String representation of the {@link PaxosPrepareReply}.
     * @return The {@link PaxosPrepareReply} represented by the String.
     */
    public static PaxosPrepareReply parseReply(String reply) {
        if (!reply.matches("(true|false) \\-?\\d+ \\-?\\d+ .+")) {
            throw new PaxosException("Invalid PaxosPrepareReply: " + reply);
        }
        
        String[] tokens = reply.split(" ", 4);
        if (tokens.length != 4) {
            throw new PaxosException("Invalid PaxosPrepareReply: " + reply);
        }
        boolean ok = Boolean.parseBoolean(tokens[0]);
        int doneValue = Integer.parseInt(tokens[1]);
        int highestAcceptNumber = Integer.parseInt(tokens[2]);
        String highestAcceptValue = tokens[3];
        
        return new PaxosPrepareReply(ok,
                                     highestAcceptNumber,
                                     highestAcceptValue,
                                     doneValue);
    }
}
