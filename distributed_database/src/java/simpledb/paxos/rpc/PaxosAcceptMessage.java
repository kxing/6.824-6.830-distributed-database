package simpledb.paxos.rpc;

import simpledb.paxos.PaxosException;
import simpledb.rpc.RpcMessageType;

public class PaxosAcceptMessage extends RpcMessageType {
    private static final String RPC_NAME = "PaxosAccept";
    
    private final int sequenceNumber;
    private final int proposalNumber;
    private final String value;
    
    public PaxosAcceptMessage(int sequenceNumber,
                              int proposalNumber,
                              String value) {
        this.sequenceNumber = sequenceNumber;
        this.proposalNumber = proposalNumber;
        this.value = value;
    }
    
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }
    
    public int getProposalNumber() {
        return this.proposalNumber;
    }
    
    public String getValue() {
        return this.value;
    }

    public String getRpcMessage() {
        return String.format("%s %d %d %s", RPC_NAME,
                                            this.sequenceNumber,
                                            this.proposalNumber,
                                            this.value);
    }

    
    /**
     * Checks whether the RPC message is a {@link PaxosAcceptMessage}.
     * 
     * @param rpcMessage The RPC message received.
     * @return Whether the RPC message is a {@link PaxosAcceptMessage}.
     */
    public static boolean matchesRpcMessage(String rpcMessage) {
        String regex = String.format("%s \\d+ \\d+ .+", RPC_NAME);
        return rpcMessage.matches(regex);
    }
    
    /**
     * Converts the RPC message into a {@link PaxosAcceptMessage}.
     * 
     * @param rpcMessage The RPC message received.
     * @return The {@link PaxosAcceptMessage} received.
     */
    public static PaxosAcceptMessage parseMessage(String rpcMessage) {
        if (!PaxosAcceptMessage.matchesRpcMessage(rpcMessage)) {
            throw new PaxosException(
                    "RPC call is not an AcceptMessage: " + rpcMessage);
        }
        
        String[] tokens = rpcMessage.split(" ", 4);
        if (tokens.length != 4) {
            throw new PaxosException(
                    "RPC call is not an AcceptMessage: " + rpcMessage);
        }
        
        int sequenceNumber = Integer.parseInt(tokens[1]);
        int proposalNumber = Integer.parseInt(tokens[2]);
        String value = tokens[3];
        
        return new PaxosAcceptMessage(sequenceNumber, proposalNumber, value);
    }
}
