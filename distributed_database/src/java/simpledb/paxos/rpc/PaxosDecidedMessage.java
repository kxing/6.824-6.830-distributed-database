package simpledb.paxos.rpc;

import simpledb.paxos.PaxosException;
import simpledb.rpc.RpcMessageType;

public class PaxosDecidedMessage extends RpcMessageType {
    private static final String RPC_NAME = "PaxosDecided";
    
    private final int sequenceNumber;
    private final String value;

    public PaxosDecidedMessage(int sequenceNumber, String value) {
        this.sequenceNumber = sequenceNumber;
        this.value = value;
    }
    
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public String getRpcMessage() {
        return String.format("%s %d %s", RPC_NAME,
                                         this.sequenceNumber,
                                         this.value);
    }

    /**
     * Checks whether the RPC message is a {@link PaxosDecidedMessage}.
     * 
     * @param rpcMessage The RPC message received.
     * @return Whether the RPC message is a {@link PaxosDecidedMessage}.
     */
    public static boolean matchesRpcMessage(String rpcMessage) {
        String regex = String.format("%s \\d+ .+", RPC_NAME);
        return rpcMessage.matches(regex);
    }

    /**
     * Converts the RPC message into a {@link PaxosDecidedMessage}.
     * 
     * @param rpcMessage The RPC message received.
     * @return The {@link PaxosDecidedMessage} received.
     */
    public static PaxosDecidedMessage parseMessage(String rpcMessage) {
        if (!PaxosDecidedMessage.matchesRpcMessage(rpcMessage)) {
            throw new PaxosException(
                    "RPC call is not a DecidedMessage: " + rpcMessage);
        }
        
        String[] tokens = rpcMessage.split(" ", 3);
        if (tokens.length != 3) {
            throw new PaxosException(
                    "RPC call is not a DecidedMessage: " + rpcMessage);
        }
        
        int sequenceNumber = Integer.parseInt(tokens[1]);
        String value = tokens[2];
        
        return new PaxosDecidedMessage(sequenceNumber, value);
    }
}
