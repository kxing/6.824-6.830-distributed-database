package simpledb.paxos.rpc;

import simpledb.paxos.PaxosException;
import simpledb.rpc.RpcResponseType;

public class PaxosDecidedReply extends RpcResponseType {
    private final int doneValue;
    
    public PaxosDecidedReply(int doneValue) {
        this.doneValue = doneValue;
    }
    
    public int getDoneValue() {
        return this.doneValue;
    }

    public String getRpcReply() {
        return Integer.toString(this.doneValue);
    }

    /**
     * Converts a String representation of a {@link PaxosDecidedReply} into a
     * {@link PaxosDecidedReply}.
     * 
     * @param reply A String representation of the {@link PaxosDecidedReply}.
     * @return The {@link PaxosDecidedReply} represented by the String.
     */
    public static PaxosDecidedReply parseReply(String reply) {
        if (!reply.matches("\\-?\\d+")) {
            throw new PaxosException("Invalid PaxosDecidedReply: " + reply);
        }
        
        int doneValue = Integer.parseInt(reply);
        
        return new PaxosDecidedReply(doneValue);
    }
}
