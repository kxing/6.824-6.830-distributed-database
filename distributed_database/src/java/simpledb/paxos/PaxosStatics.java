package simpledb.paxos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import simpledb.rpc.RpcListener;

/**
 * Stores a copy of "static" variables. This class helps make our code testable.
 * 
 * @author kxing
 */
public class PaxosStatics {
    private final PaxosConfiguration configuration;
    private final Paxos paxos;
    private boolean running;
    private int nextLogNumber;
    
    private static PaxosStatics statics;
    
    public PaxosStatics(PaxosConfiguration configuration) {
        this.configuration = configuration;
        this.paxos = new Paxos(
                this.configuration.getOtherServers(),
                this.configuration.getIndex(),
                this.configuration.getPaxosLogFileName(),
                this.configuration.getPaxosInstanceStateFileName());
        this.running = false;
        this.readPaxosLogNumber();
    }
    
    public synchronized void startRpcListener() {
        this.running = true;
        RpcListener.startRpcListener(this, this.configuration.getPort());
    }
    
    public synchronized void stopRpcListener() {
        this.running = false;
    }
    
    public synchronized boolean isRunning() {
        return this.running;
    }
    
    public synchronized int getNextLogNumber() {
        return this.nextLogNumber;
    }
    
    public synchronized void incrementNextLogNumber() {
        this.nextLogNumber++;
        this.writePaxosLogNumber();
    }
    
    public String sqlStatementToPaxosValue(String sqlStatement) {
        return String.format("%s:%d %s",
                this.configuration.getAddress(),
                this.configuration.getPort(),
                sqlStatement);
    }
    
    public String paxosValueToSqlStatement(String paxosValue) {
        if (!paxosValue.matches(".+:\\d+ .+")) {
            throw new AssertionError(
                    "Improperly formatted paxosValue: " + paxosValue);
        }
        String[] tokens = paxosValue.split(" ", 2);
        if (tokens.length != 2) {
            throw new AssertionError(
                    "Improperly formatted paxosValue: " + paxosValue);
        }
        return tokens[1];
    }
    
    public Paxos getPaxos() {
        return this.paxos;
    }
    
    public static PaxosStatics getPaxosStatics() {
        return statics;
    }
    
    public static void setPaxosStatics(PaxosStatics statics) {
        if (PaxosStatics.statics != null) {
            throw new AssertionError("PaxosStatics set more than once.");
        }
        PaxosStatics.statics = statics;
    }
    
    private void readPaxosLogNumber() {
        String paxosLogNumberFileName =
                this.configuration.getPaxosLogNumberFileName();
        try {
            // Read in the next log number.
            BufferedReader r = new BufferedReader(
                    new FileReader(paxosLogNumberFileName));
            this.nextLogNumber = Integer.parseInt(r.readLine());
            
            // Clean up.
            r.close();
        } catch (FileNotFoundException e) {
            // No file -> we assume the next log number is 0.
            this.nextLogNumber = 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void writePaxosLogNumber() {
        String paxosLogNumberFileName =
                this.configuration.getPaxosLogNumberFileName();
        
        try {
            PrintWriter w = new PrintWriter(
                    new BufferedWriter(new FileWriter(paxosLogNumberFileName)));
            w.println(this.nextLogNumber);
            
            // Clean up.
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
