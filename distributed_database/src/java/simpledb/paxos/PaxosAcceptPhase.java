package simpledb.paxos;

import java.io.IOException;
import java.util.HashMap;

import simpledb.network.NetworkAddressPort;
import simpledb.paxos.rpc.PaxosAcceptMessage;
import simpledb.paxos.rpc.PaxosAcceptReply;
import simpledb.rpc.RpcSender;

public class PaxosAcceptPhase {
    private final Paxos paxos;
    private final int sequenceNumber;
    private final int proposalNumber;
    private final String value;
    
    private int acceptOks;
    
    private final HashMap<NetworkAddressPort, Integer> otherDoneValues;

    public PaxosAcceptPhase(Paxos paxos,
                            int sequenceNumber,
                            int proposalNumber,
                            String value) {
        this.paxos = paxos;
        this.sequenceNumber = sequenceNumber;
        this.proposalNumber = proposalNumber;
        this.value = value;
        
        this.acceptOks = 0;
        
        this.otherDoneValues = new HashMap<NetworkAddressPort, Integer>();
    }
    
    public boolean isSuccess() {
        int totalServers = this.paxos.getOtherPaxosServers().length + 1;
        return (this.acceptOks * 2 > totalServers);
    }
    
    private void runAcceptPhase() {
        // Construct the accept message.
        PaxosAcceptMessage acceptMessage =
            new PaxosAcceptMessage(this.sequenceNumber,
                                   this.proposalNumber,
                                   this.value);
        
        // Make an RPC to everybody else.
        for (NetworkAddressPort otherServer :
                this.paxos.getOtherPaxosServers()) {
            PaxosAcceptReply acceptReply = null;
            
            try {
                acceptReply = RpcSender.makePaxosAcceptRpc(otherServer,
                                                           acceptMessage);
                this.handleReply(acceptReply);
                
                this.otherDoneValues.put(otherServer,
                                         acceptReply.getDoneValue());
            } catch (IOException e) {
                // RPCs can fail, so we'll ignore the exception.
            }
        }

        // Make a local RPC.
        PaxosAcceptReply localAcceptReply =
                this.paxos.handleAcceptRpc(acceptMessage);
        this.handleReply(localAcceptReply);
        
        // Update the done values.
        this.paxos.updateOtherDoneValues(this.otherDoneValues);
    }
    
    private void handleReply(PaxosAcceptReply acceptReply) {
        if (acceptReply.isOk()) {
            this.acceptOks++;
        }
    }
    
    public static PaxosAcceptPhase doAcceptPhase(Paxos paxos,
                                                 int sequenceNumber,
                                                 int proposalNumber,
                                                 String value) {
        PaxosAcceptPhase acceptPhase = new PaxosAcceptPhase(paxos,
                                                            sequenceNumber,
                                                            proposalNumber,
                                                            value);
        acceptPhase.runAcceptPhase();
        return acceptPhase;
    }
}
