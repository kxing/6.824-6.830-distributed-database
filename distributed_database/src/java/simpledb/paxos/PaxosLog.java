package simpledb.paxos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * A class to store the agreed-upon values in the Paxos log.
 * 
 * @author kxing
 */
public class PaxosLog {
    /**
     * A map of sequence numbers to Paxos values.
     */
    private final HashMap<Integer, String> logItems;
    
    private final String paxosLogFileName;

    public PaxosLog(String paxosLogFileName) {
        this.logItems = new HashMap<Integer, String>();
        this.paxosLogFileName = paxosLogFileName;
        
        this.readFromFile();
    }
    
    /**
     * Adds a {@link PaxosLogItem} to the Paxos log.
     * Throws a {@link PaxosException} if an item with the same sequence number
     * is already present in the log.
     * 
     * @param logItem The {@link PaxosLogItem} to add.
     */
    public void addToLog(int sequenceNumber, String value) {
        if (this.logItems.containsKey(sequenceNumber)) {
            throw new PaxosException("Trying to add a log item whose " +
            		                 "sequence number is already taken.");
        }
        
        this.logItems.put(sequenceNumber, value);
        this.writeToFile(sequenceNumber, value);
    }
    
    public boolean containsSequenceNumber(int sequenceNumber) {
        return this.logItems.containsKey(sequenceNumber);
    }
    
    /**
     * Gets the value associated with the sequence number.
     * If the item is not in the log, a {@link PaxosException} exception is
     * thrown.
     * 
     * @param sequenceNumber The sequence number of the log item.
     * @return The value associated with the sequence number.
     */
    public String getValue(int sequenceNumber) {
        if (!this.containsSequenceNumber(sequenceNumber)) {
            throw new PaxosException(
                    String.format("Sequence number not found: %d",
                                  sequenceNumber));
        }
        return this.logItems.get(sequenceNumber);
    }
    
    /**
     * Forgets the value associated with the sequence number.
     * If the item is not in the log, a {@link PaxosException} exception is
     * thrown.
     * 
     * @param sequenceNumber The sequence number of the log item.
     */
    public void forgetValue(int sequenceNumber) {
        if (!this.containsSequenceNumber(sequenceNumber)) {
            throw new PaxosException(
                    String.format("Sequence number  %d is not in the Paxos log",
                                  sequenceNumber));
        }
        this.logItems.remove(sequenceNumber);
    }
    
    private void readFromFile() {
        this.logItems.clear();
        try {
            BufferedReader r = new BufferedReader(
                    new FileReader(this.paxosLogFileName));
            String line = r.readLine();
            
            while (line != null) {
                // Process the line.
                String[] tokens = line.split(" ", 2);
                if (tokens.length != 2) {
                    throw new AssertionError(
                            "Improperly formatted line: " + line);
                }
                int sequenceNumber = Integer.parseInt(tokens[0]);
                String value = tokens[1];
                
                // Add the (sequence number, value) pair to the Paxos log.
                this.logItems.put(sequenceNumber, value);
                
                line = r.readLine();
            }
            r.close();
        } catch (FileNotFoundException e) {
            // The file may not exist, which is okay.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void writeToFile(int sequenceNumber, String value) {
        try {
            PrintWriter w = new PrintWriter(new BufferedWriter(new FileWriter(
                    this.paxosLogFileName, true)));
            w.println(String.format("%d %s", sequenceNumber, value));
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
