package simpledb.paxos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import simpledb.network.NetworkAddressPort;
import simpledb.paxos.rpc.PaxosAcceptMessage;
import simpledb.paxos.rpc.PaxosAcceptReply;
import simpledb.paxos.rpc.PaxosDecidedMessage;
import simpledb.paxos.rpc.PaxosDecidedReply;
import simpledb.paxos.rpc.PaxosPrepareMessage;
import simpledb.paxos.rpc.PaxosPrepareReply;

/**
 * A class to coordinate Paxos agreements.
 * 
 * @author kxing
 */
public class Paxos {
    
    private final NetworkAddressPort[] otherPaxosServers;
    private final int index;
    private final HashMap<Integer, PaxosInstanceState> instanceStates;
    private final PaxosLog paxosLog;
    
    private final HashMap<NetworkAddressPort, Integer> otherDoneValues;
    private int myDoneValue;
    
    private final String paxosInstanceStateFileName;

    public Paxos(NetworkAddressPort[] otherPaxosServers,
                 int index,
                 String paxosLogFileName,
                 String paxosInstanceStateFileName) {
        this.otherPaxosServers = otherPaxosServers;
        this.index = index;
        this.instanceStates = new HashMap<Integer, PaxosInstanceState>();
        this.paxosLog = new PaxosLog(paxosLogFileName);
        
        this.otherDoneValues = new HashMap<NetworkAddressPort, Integer>();
        this.myDoneValue = -1;
        
        for (NetworkAddressPort otherServer : this.otherPaxosServers) {
            this.otherDoneValues.put(otherServer, -1);
        }
        
        this.paxosInstanceStateFileName = paxosInstanceStateFileName;
        
        this.readPaxosInstanceState();
    }
    
    public NetworkAddressPort[] getOtherPaxosServers() {
        return this.otherPaxosServers;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public synchronized boolean containsSequenceNumber(int sequenceNumber) {
        return this.paxosLog.containsSequenceNumber(sequenceNumber);
    }
    
    public synchronized String getPaxosLogValue(int sequenceNumber) {
        if (this.paxosLog.containsSequenceNumber(sequenceNumber)) {
            return this.paxosLog.getValue(sequenceNumber);
        }
        return null;
    }
    
    /**
     * Forgets the sequence number up to and including the sequence number.
     * 
     * @param sequenceNumber The sequence number of the last entry to be
     *                       forgotten.
     */
    public synchronized void forgetValues(int sequenceNumber) {
        int oldMinimum = this.getDoneValueMinimum();
        
        if (this.myDoneValue < sequenceNumber) {
            this.myDoneValue = sequenceNumber;
        }
        
        int newMinimum = this.getDoneValueMinimum();
        
        if (oldMinimum < newMinimum) {
            this.garbageCollect(oldMinimum, newMinimum);
        }
    }
    
    public synchronized void updateOtherDoneValues(
            HashMap<NetworkAddressPort, Integer> otherDoneValues) {
        int oldMinimum = this.getDoneValueMinimum();
        
        for (NetworkAddressPort otherServer : otherDoneValues.keySet()) {
            int doneValue = otherDoneValues.get(otherServer);
            
            if (this.otherDoneValues.get(otherServer) < doneValue) {
                this.otherDoneValues.put(otherServer, doneValue);
            }
        }
        
        int newMinimum = this.getDoneValueMinimum();
        
        if (oldMinimum < newMinimum) {
            this.garbageCollect(oldMinimum, newMinimum);
        }
    }
    
    public synchronized void startPaxos(int sequenceNumber, String value) {
        if (this.paxosLog.containsSequenceNumber(sequenceNumber)) {
            // There's no point - the entry is already determined in the log.
            return;
        }

        // Spawn a new thread to get the Paxos servers to reach an agreement.
        Thread t = new Thread(new PaxosSequenceRunner(this,
                                                      sequenceNumber,
                                                      value));
        t.start();
    }
    
    /**
     * Handler for the Paxos Prepare RPC.
     * 
     * @param prepareMessage The {@link PaxosPrepareMessage} associated with the
     *                       RPC.
     * @return The {@link PaxosPrepareReply} as the RPC response.
     */
    public synchronized PaxosPrepareReply handlePrepareRpc(
            PaxosPrepareMessage prepareMessage) {
        int sequenceNumber = prepareMessage.getSequenceNumber();
        int proposalNumber = prepareMessage.getProposalNumber();
        
        if (!this.instanceStates.containsKey(sequenceNumber)) {
            PaxosInstanceState instanceState = new PaxosInstanceState();
            
            // Initialize the instance state, as necessary from the Paxos log.
            if (this.paxosLog.containsSequenceNumber(sequenceNumber)) {
                String paxosLogValue = this.paxosLog.getValue(sequenceNumber);
                
                instanceState.setHighestAcceptNumber(Integer.MAX_VALUE);
                instanceState.setHighestAcceptValue(paxosLogValue);
            }
            this.instanceStates.put(sequenceNumber, instanceState);
        }
        
        // The state will get updated by reference.
        PaxosInstanceState state = this.instanceStates.get(sequenceNumber);
        
        boolean replyResult;
        if (proposalNumber > state.getHighestPrepareNumber()) {
            state.setHighestPrepareNumber(proposalNumber);
            replyResult = true;
        } else {
            replyResult = false;
        }
        
        this.writePaxosInstanceStates();
        
        return new PaxosPrepareReply(replyResult,
                                     state.getHighestAcceptNumber(),
                                     state.getHighestAcceptValue(),
                                     this.myDoneValue);
    }
    
    /**
     * Handler for the Paxos Accept RPC.
     * 
     * @param acceptMessage The {@link PaxosAcceptMessage} associated with the
     *                      RPC.
     * @return The {@link PaxosAcceptReply} as the RPC response.
     */
    public synchronized PaxosAcceptReply handleAcceptRpc(
            PaxosAcceptMessage acceptMessage) {
        int sequenceNumber = acceptMessage.getSequenceNumber();
        int proposalNumber = acceptMessage.getProposalNumber();
        String value = acceptMessage.getValue();

        if (!this.instanceStates.containsKey(sequenceNumber)) {
            this.instanceStates.put(sequenceNumber, new PaxosInstanceState());
        }
        
        // The state will get updated by reference.
        PaxosInstanceState state = this.instanceStates.get(sequenceNumber);
        
        boolean replyResult;
        if (proposalNumber >= state.getHighestPrepareNumber()) {
            state.setHighestPrepareNumber(proposalNumber);
            state.setHighestAcceptNumber(proposalNumber);
            state.setHighestAcceptValue(value);
            
            replyResult = true;
        } else {
            replyResult = false;
        }
        
        this.writePaxosInstanceStates();
        
        return new PaxosAcceptReply(replyResult, this.myDoneValue);
    }
    
    public synchronized PaxosDecidedReply handleDecidedRpc(
            PaxosDecidedMessage decidedMessage) {
        int sequenceNumber = decidedMessage.getSequenceNumber();
        String value = decidedMessage.getValue();
        
        // Sanity check.
        if (this.paxosLog.containsSequenceNumber(sequenceNumber)) {
            String storedValue =
                    this.paxosLog.getValue(sequenceNumber);
            if (!storedValue.equals(value)) {
                throw new PaxosException(String.format(
                        "Inconsistent values with sequence number " +
                        "%d: %s and %s", sequenceNumber, storedValue, value));
            }
        }
        
        // Add to the log, if we just learned something.
        if (!this.paxosLog.containsSequenceNumber(sequenceNumber)) {
            this.paxosLog.addToLog(sequenceNumber, value);
        }
        
        return new PaxosDecidedReply(this.myDoneValue);
    }
    
    private synchronized int getDoneValueMinimum() {
        int minimum = this.myDoneValue;
        for (NetworkAddressPort otherServer : this.otherDoneValues.keySet()) {
            int doneValue = this.otherDoneValues.get(otherServer);
            
            if (minimum > doneValue) {
                minimum = doneValue;
            }
        }
        return minimum;
    }
    
    private synchronized void garbageCollect(int oldMinimum, int newMinimum) {
        for (int i = oldMinimum + 1; i <= newMinimum; i++) {
            // Forget about the corresponding entries in the Paxos log.
            if (this.paxosLog.containsSequenceNumber(i)) {
                this.paxosLog.forgetValue(i);
            }
            
            // Forget about the corresponding instance states.
            if (this.instanceStates.containsKey(i)) {
                this.instanceStates.remove(i);
            }
        }
        this.writePaxosInstanceStates();
    }
    
    private synchronized void writePaxosInstanceStates() {
        try {
            PrintWriter w = new PrintWriter(new BufferedWriter(new FileWriter(
                    this.paxosInstanceStateFileName)));
            for (Map.Entry<Integer, PaxosInstanceState> entry
                    : this.instanceStates.entrySet()) {
                PaxosInstanceState instanceState = entry.getValue();
                w.println(String.format("%d %d %d %s",
                          entry.getKey(),
                          instanceState.getHighestPrepareNumber(),
                          instanceState.getHighestAcceptNumber(),
                          instanceState.getHighestAcceptValue()));
            }
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private synchronized void readPaxosInstanceState() {
        this.instanceStates.clear();
        try {
            BufferedReader r = new BufferedReader(new FileReader(
                    this.paxosInstanceStateFileName));
            String line = r.readLine();
            while (line != null) {
                String[] tokens = line.split(" ", 4);
                if (tokens.length != 4) {
                    throw new AssertionError("Invalid line: " + line);
                }
                int sequenceNumber = Integer.parseInt(tokens[0]);
                int highestPrepareNumber = Integer.parseInt(tokens[1]);
                int highestAcceptNumber = Integer.parseInt(tokens[2]);
                String highestAcceptValue = tokens[3];
                
                // Add in the instance state.
                PaxosInstanceState instanceState = new PaxosInstanceState();
                instanceState.setHighestPrepareNumber(highestPrepareNumber);
                instanceState.setHighestAcceptNumber(highestAcceptNumber);
                instanceState.setHighestAcceptValue(highestAcceptValue);
                
                this.instanceStates.put(sequenceNumber, instanceState);
                
                line = r.readLine();
            }
            r.close();
        } catch (FileNotFoundException e) {
            // It's possible that the file does not exist.
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
