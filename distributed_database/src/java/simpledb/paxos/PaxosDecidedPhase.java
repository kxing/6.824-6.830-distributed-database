package simpledb.paxos;

import java.io.IOException;
import java.util.HashMap;

import simpledb.network.NetworkAddressPort;
import simpledb.paxos.rpc.PaxosDecidedMessage;
import simpledb.paxos.rpc.PaxosDecidedReply;
import simpledb.rpc.RpcSender;

public class PaxosDecidedPhase {
    private final Paxos paxos;
    private final int sequenceNumber;
    private final String value;
    
    private final HashMap<NetworkAddressPort, Integer> otherDoneValues;

    public PaxosDecidedPhase(Paxos paxos,
                             int sequenceNumber,
                             String value) {
        this.paxos = paxos;
        this.sequenceNumber = sequenceNumber;
        this.value = value;
        
        this.otherDoneValues = new HashMap<NetworkAddressPort, Integer>();
    }
    
    private void runDecidedPhase() {
        // Construct the decided message.
        PaxosDecidedMessage decidedMessage =
                new PaxosDecidedMessage(this.sequenceNumber, this.value);
        
        // Make an RPC to everybody else.
        for (NetworkAddressPort otherServer :
                this.paxos.getOtherPaxosServers()) {
            try {
                PaxosDecidedReply decidedReply =
                        RpcSender.makePaxosDecidedRpc(otherServer,
                                                      decidedMessage);
                this.otherDoneValues.put(otherServer,
                                         decidedReply.getDoneValue());
            } catch (IOException e) {
                // RPCs can fail, so we'll ignore the exception.
            }
        }

        // Make a local RPC.
        this.paxos.handleDecidedRpc(decidedMessage);
        
        // Upate the done values.
        this.paxos.updateOtherDoneValues(this.otherDoneValues);
    }
    
    public static void doDecidedPhase(Paxos paxos,
                                      int sequenceNumber,
                                      String value) {
        PaxosDecidedPhase decidedPhase = new PaxosDecidedPhase(paxos,
                                                               sequenceNumber,
                                                               value);
        decidedPhase.runDecidedPhase();
    }
}
