package simpledb.paxos;

public class PaxosException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public PaxosException(String message) {
        super(message);
    }
}
