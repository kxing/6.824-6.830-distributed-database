package simpledb.paxos;

import java.io.IOException;
import java.util.HashMap;

import simpledb.network.NetworkAddressPort;
import simpledb.paxos.rpc.PaxosPrepareMessage;
import simpledb.paxos.rpc.PaxosPrepareReply;
import simpledb.rpc.RpcSender;

public class PaxosPreparePhase {
    private final Paxos paxos;
    private final int sequenceNumber;
    private final int proposalNumber;
    private final String initialValue;

    private int prepareOks;
    private int bestProposalNumber;
    private String bestProposalValue;
    
    private final HashMap<NetworkAddressPort, Integer> otherDoneValues;
    
    private PaxosPreparePhase(Paxos paxos,
                              int sequenceNumber,
                              int proposalNumber,
                              String value) {
        this.paxos = paxos;
        this.sequenceNumber = sequenceNumber;
        this.proposalNumber = proposalNumber;
        this.initialValue = value;
        
        this.prepareOks = 0;
        this.bestProposalNumber = -1;
        this.bestProposalValue = this.initialValue;
        
        this.otherDoneValues = new HashMap<NetworkAddressPort, Integer>();
    }
    
    public boolean isSuccess() {
        int totalServers = this.paxos.getOtherPaxosServers().length + 1;
        return (this.prepareOks * 2 > totalServers);
    }
    
    public int getBestProposalNumber() {
        return this.bestProposalNumber;
    }
    
    public String getBestProposalValue() {
        return this.bestProposalValue;
    }
    
    private void runPreparePhase() {
        // Construct the proposal message.
        PaxosPrepareMessage prepareMessage =
                new PaxosPrepareMessage(this.sequenceNumber,
                                        this.proposalNumber);
        
        // Make an RPC to everybody else.
        for (NetworkAddressPort otherServer :
                this.paxos.getOtherPaxosServers()) {
            PaxosPrepareReply prepareReply = null;
            
            try {
                prepareReply = RpcSender.makePaxosPrepareRpc(otherServer,
                                                             prepareMessage);
                this.handleReply(prepareReply);
                this.otherDoneValues.put(otherServer,
                                         prepareReply.getDoneValue());
            } catch (IOException e) {
                // RPCs can fail, so we'll ignore the exception.
            }
        }
        
        // Make a local RPC.
        PaxosPrepareReply localPrepareReply =
                this.paxos.handlePrepareRpc(prepareMessage);
        this.handleReply(localPrepareReply);
        
        // Update the done values.
        this.paxos.updateOtherDoneValues(this.otherDoneValues);
    }
    
    private void handleReply(PaxosPrepareReply prepareReply) {
        if (prepareReply.isOk()) {
            prepareOks++;
            
            int highestAcceptNumber = prepareReply.getHighestAcceptNumber();
            String highestAcceptValue = prepareReply.getHighestAcceptValue();
            
            if (highestAcceptNumber > bestProposalNumber) {
                bestProposalNumber = highestAcceptNumber;
                bestProposalValue = highestAcceptValue;
            }
        }
    }
    
    public static PaxosPreparePhase doPreparePhase(Paxos paxos,
                                                   int sequenceNumber,
                                                   int proposalNumber,
                                                   String value) {
        PaxosPreparePhase preparePhase = new PaxosPreparePhase(paxos,
                                                               sequenceNumber,
                                                               proposalNumber,
                                                               value);
        preparePhase.runPreparePhase();
        return preparePhase;
    }
}
