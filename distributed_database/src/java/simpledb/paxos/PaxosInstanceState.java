package simpledb.paxos;

public class PaxosInstanceState {
    private int highestPrepareNumber;
    private int highestAcceptNumber;
    private String highestAcceptValue;
    
    private static int NOT_SET = -1;
    private static String NONE = "(none)";
    
    public PaxosInstanceState() {
        this.highestPrepareNumber = NOT_SET;
        this.highestAcceptNumber = NOT_SET;
        this.highestAcceptValue = NONE;
    }
    
    public int getHighestPrepareNumber() {
        return this.highestPrepareNumber;
    }
    
    public void setHighestPrepareNumber(int highestPrepareNumber) {
        this.highestPrepareNumber = highestPrepareNumber;
    }
    
    public int getHighestAcceptNumber() {
        return this.highestAcceptNumber;
    }
    
    public void setHighestAcceptNumber(int highestAcceptNumber) {
        this.highestAcceptNumber = highestAcceptNumber;
    }
    
    public String getHighestAcceptValue() {
        return this.highestAcceptValue;
    }
    
    public void setHighestAcceptValue(String highestAcceptValue) {
        this.highestAcceptValue = highestAcceptValue;
    }
}
